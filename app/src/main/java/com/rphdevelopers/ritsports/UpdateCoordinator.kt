package com.rphdevelopers.ritsports

import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.json.JSONObject

class UpdateCoordinator : AppCompatActivity() {

    lateinit var btnSave: Button
    lateinit var etName: EditText
    lateinit var etMob: EditText
    lateinit var email: String
    lateinit var name: String
    lateinit var mobile: String

    val const: Const = Const()
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_coordinator)

        sharedPreferences = this.getSharedPreferences(const.SHARED_PREF_NAME,Context.MODE_PRIVATE)

        btnSave = findViewById(R.id.btn_uc_save)
        etName = findViewById(R.id.et_uc_name)
        etMob = findViewById(R.id.et_uc_mobile)

        btnSave.setOnClickListener {
            if(etName.text.toString() == "")
                findViewById<TextInputLayout>(R.id.til_uc_name).error = "Name required"
            else if(etMob.text.toString()=="")
                findViewById<TextInputLayout>(R.id.til_uc_mobile).error = "Mobile number required"
            else saveData()
        }
    }

    private fun saveData() {
        name = etName.text.toString()
        mobile = etMob.text.toString()
        email = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()

        val stringRequest = object : StringRequest(
            Method.POST, const.UPDATE_CDATA_URL,
            Response.Listener<String> { response ->
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        val editor = sharedPreferences.edit()
                        editor.putBoolean(const.LOGGEDIN_SHARED_PREF, false)
                        editor.putString(const.EMAIL_SHARED_PREF, null)
                        editor.putString(const.LOGGEDIN_ROLE_SHARED_PREF,null)
                        editor.putString(const.STATUS_SHARED_PREF,null)
                        editor.commit()
                        Toast.makeText(applicationContext, "Updated...",Toast.LENGTH_LONG).show()
                        finish()
                    } else {
                        Toast.makeText(applicationContext,response,Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext, "Exception$response", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params[const.USER_EMAIL] = email
                params[const.USER_NAME] = name
                params["mobile"] = mobile
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(this)
        requestQueue.add(stringRequest)

    }
}
