package com.rphdevelopers.ritsports


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.fragment_add_players.*
import kotlinx.android.synthetic.main.fragment_add_score_updater.*

/**
 * A simple [Fragment] subclass.
 */
class FragmentAddScoreUpdater : Fragment() {
    lateinit var etName: EditText
    lateinit var etPassword: EditText
    lateinit var etCPassword: EditText
    lateinit var etEmail: EditText
    lateinit var btnAdd: Button
    lateinit var rootView: View
    lateinit var prefs: SharedPreferences
    val const = Const()
    val validator = Validator()
    val volleyRequests = VolleyRequests()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_score_updater, container, false)
        initializeObjects()
        clearInputError()

        //ADD on click listner
        btnAdd.setOnClickListener {
            if(isAllGiven()){
                var params = HashMap<String, String>()
                params["email"] = etEmail.text.toString()
                params["cemail"] = prefs.getString(const.EMAIL_SHARED_PREF,null).toString()
                params["password"] = etPassword.text.toString()
                params["name"] = etName.text.toString()
                volleyRequests.insert(const.ADD_SCORE_UPDATER_URL,rootView,params,"Added...")
            }
        }
        return rootView
    }

    //TO CHECK ALL VALUE IS GIVNE OR NOT
    private fun isAllGiven(): Boolean {
        var flag = true
        if(etName.text.isEmpty()){
            til_as_name.error = "Name required"
            flag = false
        }
        if(etEmail.text.isEmpty()){
            til_as_email.error = "Email required"
            flag = false
        }else if(!validator.isEmailValid(etEmail.text.toString())){
            til_as_email.error = "No a valid email"
            flag = false
        }
        if(etPassword.text.isEmpty()){
            til_as_password.error = "Enter a password"
            flag = false
        }
        if (etCPassword.text.isEmpty()){
            til_as_cpassword.error = "Enter confirm password"
            flag = false
        }
        if(flag){
            if(etPassword.text.toString() != etCPassword.text.toString()){
                til_as_cpassword.error = "Password not match"
                flag =false
            }
        }
        return flag
    }

    //TO INITIALIZE ALL OBJECTS USED IN THIS
    private fun initializeObjects() {
        etName = rootView.findViewById(R.id.et_as_name)
        etPassword = rootView.findViewById(R.id.et_as_password)
        etCPassword = rootView.findViewById(R.id.et_as_cpassword)
        etEmail = rootView.findViewById(R.id.et_as_email)
        btnAdd = rootView.findViewById(R.id.btn_as_add)
        prefs = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME,Context.MODE_PRIVATE)
    }

    //TO CLEAR THE INPUT LAYOUT ERROR TEXT
    private fun clearInputError(){
        etName.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_as_name.error = ""
            }

        })
        etEmail.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_as_email.error = ""
            }

        })
        etPassword.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_as_password.error = ""
            }

        })
        etCPassword.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_as_cpassword.error = ""
            }

        })
    }

}
