package com.rphdevelopers.ritsports

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import java.time.Duration
import java.util.*
import android.content.Intent
import android.widget.*
import java.text.SimpleDateFormat


class Display{
    //TO CREATE A CUSTOM TOAST TO SHOW ON SUCCESS
    fun showSuccessToast(rootView: View,message: String, gravity: Int, x: Int, y: Int){
        val inflater: LayoutInflater = rootView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = inflater.inflate(R.layout.toast_success,(rootView.context as Activity).findViewById(R.id.custom))
        var toast = Toast(rootView.context)
        toast.createToast(layout,rootView.context,message, gravity, x, y)
    }
    fun showErrorToast(rootView: View,message: String, gravity: Int, x: Int, y: Int){
        val inflater: LayoutInflater = rootView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = inflater.inflate(R.layout.toast_failure,(rootView.context as Activity).findViewById(R.id.custom))
        var toast = Toast(rootView.context)
        toast.createToast(layout,rootView.context,message, gravity, x, y)
    }
    private fun Toast.createToast(layout: View,context: Context, message: String, gravity: Int, x: Int, y: Int){
        layout.findViewById<TextView>(R.id.tv_success).text = message
        setGravity(gravity, x, y)
        duration = Toast.LENGTH_LONG
        view = layout
        show()
    }


    //FOR DATE PICKER DIALOG
    class DatePickerFragment(view: View,btn: Button) : DialogFragment(), DatePickerDialog.OnDateSetListener {
        var v: View = view
        var btnDate: Button = btn
        private val c: Calendar = Calendar.getInstance()

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            // Use the current date as the default date in the picker
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            // Create a new instance of DatePickerDialog and return it
            return DatePickerDialog(v.context, this, year, month, day)
        }

        override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
            val selectedDate = "$day/$month/$year"
            btnDate.text = selectedDate
        }
    }


}