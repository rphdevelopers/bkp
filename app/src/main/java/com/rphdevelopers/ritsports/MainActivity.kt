package com.rphdevelopers.ritsports

import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.navigation.NavigationView
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var headerView: View
    lateinit var dialog: Dialog
    lateinit var const: Const
    lateinit var navView: NavigationView
    lateinit var prefs: SharedPreferences
    lateinit var btnSignIn: ImageView
    lateinit var btnSignOut: ImageView
    lateinit var appbarLayout: AppBarLayout
    lateinit var imgCollaps: ImageView
    lateinit var tvDrawerName: TextView
    lateinit var ivCollapse: ImageView
    lateinit var collapsingToolbar: CollapsingToolbarLayout
    lateinit var toolbar: Toolbar
    lateinit var snackbar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ll_content_main.visibility = View.VISIBLE
        //DECLARATIONS
        appbarLayout = findViewById(R.id.appBarLayout)
        imgCollaps = findViewById(R.id.imgCollaps)
        navView = findViewById(R.id.nav_view)
        headerView = navView.getHeaderView(0)
        btnSignOut = headerView.findViewById(R.id.btn_sign_out)
        btnSignIn = headerView.findViewById(R.id.btn_sign_in)
        tvDrawerName = headerView.findViewById(R.id.tv_drawer_name)
        ivCollapse = findViewById(R.id.imgCollaps)
        collapsingToolbar = findViewById(R.id.collapsing_toolbar_layout)
        toolbar = findViewById(R.id.toolbar)


        //for snack bar
        val dra = findViewById<CoordinatorLayout>(R.id.main_coord_layout)
        snackbar = Snackbar.make(dra,"Waiting for team approval",Snackbar.LENGTH_INDEFINITE)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        dialog = Dialog(applicationContext)
        const = Const()

        //TO CHECK USER LOGGED IN OR NOT
        setUser()

        //TO SET NAVIGATION DRAWER
        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(navController.graph,
            drawerLayout = findViewById(R.id.main_drawer_layout))
        navView.setupWithNavController(navController)
        collapsingToolbar.setupWithNavController(toolbar, navController,appBarConfiguration)




        //ON FRAGMENT CHANGE -- for pin the collapsing toolbar
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if(destination.id != R.id.fragment_home) { //SET EXCEPT FOR THE HOME PAGE
                collapsingToolbar.isTitleEnabled = false
                val img: ImageView = findViewById(R.id.imgCollaps)
                img.setImageResource(0)
                when(destination.id){
                    R.id.fragment_add_coordinator -> toolbar.title = "Add Tournament"
                    R.id.fragment_coordinators_list -> toolbar.title = "Coordinators"
                    R.id.fragment_add_tournament -> toolbar.title = "Add Tournament"
                    R.id.fragment_change_password -> toolbar.title = "Change Password"
                    R.id.fragmentMyTournament -> toolbar.title = "My Tournament"
                    R.id.fragmentRegisterTeam -> toolbar.title = "Team registration"
                    R.id.fragmentTeamRequests -> toolbar.title = "Team Requests"
                    R.id.fragmentAddPlayers -> {
                        prefs = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                        if(prefs.getBoolean(const.IS_PLAYER_ADDED_SHARED_PREF,false))
                            toolbar.title = "Edit players"
                        else
                            toolbar.title = "Add players"
                    }
                    R.id.fragmentMyPlayers -> toolbar.title = "My players"
                    R.id.fragmentAddScoreUpdater -> toolbar.title = "Add Score updater"
                    R.id.fragmentMyScoreUpdater -> toolbar.title = "My Score updater"
                    R.id.fragmentTeams -> toolbar.title = "Teams"
                }
            }else{
                collapsingToolbar.isTitleEnabled = true
                val img: ImageView = findViewById(R.id.imgCollaps)
                img.setImageResource(R.drawable.collaps)
            }
        }

        //TO ADD ONCLICK FOR SIGN IN BUTTON IN DRAWER
        btnSignIn.setOnClickListener {
            intent = Intent(this,SignInActivity::class.java)
            startActivity(intent)
        }
        //LOGOUT
        btnSignOut.setOnClickListener {
            prefs = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
            val editor = prefs.edit()
            editor.putBoolean(const.LOGGEDIN_SHARED_PREF, false)
            editor.putString(const.LOGGEDIN_ROLE_SHARED_PREF, null)
            editor.putBoolean(const.IS_SCHEDULE_ADDED_SHARED_PREF,false)
            editor.commit()
            setUser()
        }
    }

    //TO SET THE LOGGED USER
    private fun setUser() {
        var menu: Int = R.menu.nav_menu
        prefs = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)

        if (prefs.getBoolean(const.LOGGEDIN_SHARED_PREF, false)) { //user logged in before
            setLogOutButton()            //TO VIEW LOGOUT BUTTON AND HIDE LOGIN
            tvDrawerName.setText(prefs.getString(const.EMAIL_SHARED_PREF, const.USER).toString())
            val role: String = prefs.getString(const.LOGGEDIN_ROLE_SHARED_PREF,null).toString()
            val status: Int = prefs.getString(const.STATUS_SHARED_PREF,null).toString().toInt()
            //FOR CHANGE MENU BASED ON USER ROLE

            when(role){
                const.ROLE_ADMIN -> {
                    menu = R.menu.nav_admin_menu
                    snackbar.dismiss()
                }
                const.ROLE_TOUR_COORDINATOR -> when(status){
                    1 -> {
                        menu = if(prefs.getBoolean(const.IS_SCHEDULE_ADDED_SHARED_PREF,false))
                            R.menu.nav_coordinator_menu_update
                        else R.menu.nav_coordinator_menu
                        snackbar.dismiss()
                    }
                    -1 -> {
                        snackbar.dismiss()
                        intent = Intent(this, UpdateCoordinator::class.java)
                        startActivity(intent)
                    }
                }
                const.ROLE_TEAM_MANAGER -> when(status){
                    1 -> {
                        menu = if(prefs.getBoolean(const.IS_PLAYER_ADDED_SHARED_PREF,false))
                            R.menu.nav_team_manager_update_menu
                            else R.menu.nav_team_manager_menu
                        snackbar.dismiss()
                    }
                    -1 -> {
                        R.menu.nav_menu
                        snackbar.show()
                    }
                }
                else ->{
                    menu = R.menu.nav_menu
                    snackbar.dismiss()
                }
            }
            setNavMenu(menu)
        } else {
            tvDrawerName.text = const.USER
            setLogInButton()            //TO VIEW LOGIN BUTTON AND HIDE LOGOUT
            setNavMenu(menu)
        }
    }

    private fun setLogInButton() {
        btnSignIn.visibility = View.VISIBLE
        btnSignIn.isClickable = true

        btnSignOut.isClickable = false
        btnSignOut.visibility = View.GONE
    }

    private fun setLogOutButton() {
        btnSignOut.visibility = View.VISIBLE
        btnSignOut.isClickable = true

        btnSignIn.isClickable = false
        btnSignIn.visibility = View.GONE
    }

    private fun setNavMenu(menu: Int) {
        navView.menu.clear()
        navView.inflateMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        setUser()
    }
    //TO CHANGE FRAGMENT ON MENU ITEM CLICK
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


}
