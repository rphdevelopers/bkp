package com.rphdevelopers.ritsports


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ShareActionProvider
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class FragmentTeamRequests : Fragment(), TeamRequestAdapter.OnItemListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var const: Const
    private lateinit var nameArray: ArrayList<String>
    private lateinit var mobileArray: ArrayList<String>
    private lateinit var teamNameArray: ArrayList<String>
    private lateinit var emailArray: ArrayList<String>
    private lateinit var semArray: ArrayList<String>
    private lateinit var departmentArray: ArrayList<String>
    private lateinit var rootView: View
    private lateinit var lprogressHorizontal: View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_team_requests, container, false)
        const = Const()
        lprogressHorizontal = rootView.findViewById<View>(R.id.pb_tr_loading)

        //FOR RECYCLER VIEW DATA
        viewManager = LinearLayoutManager(rootView.context)
        getTeamRequests()
        return rootView
    }

    //TO RETRIEVE THE TEAM REQUEST TO LIST RECUCLER VIEW
    private fun getTeamRequests() {
        lprogressHorizontal.visibility = View.VISIBLE

        //call the webservice
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_TEAM_REQUEST,
            Response.Listener<String> { response ->
                Log.e("rq",response)
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        lprogressHorizontal.visibility = View.GONE
                        initializeArray()
                        var jsonArray: JSONArray = jsonObject.getJSONArray("requests")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("user_name")
                            val department = jsonObject1.getString("dep_name")
                            val sem = jsonObject1.getString("semester")
                            val email = jsonObject1.getString("user_email")
                            val mobile = jsonObject1.getString("user_mob")
                            val teamName = jsonObject1.getString("team_name")
                            nameArray.add(name)
                            departmentArray.add(department)
                            semArray.add(sem)
                            emailArray.add(email)
                            mobileArray.add(mobile)
                            teamNameArray.add(teamName)
                        }
                        viewAdapter = TeamRequestAdapter(semArray,departmentArray,teamNameArray,nameArray,mobileArray,emailArray
                        ,rootView.context, lprogressHorizontal,this)
                        recyclerView = rootView.findViewById<RecyclerView>(R.id.rv_tr_team_requests).apply{
                            layoutManager = viewManager
                            adapter = viewAdapter
                        }
                        recyclerView.addItemDecoration(
                            DividerItemDecoration(recyclerView.context,
                                DividerItemDecoration.VERTICAL)
                        )
                    }
                } catch (e: Exception) {
                    lprogressHorizontal.visibility = View.GONE
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                lprogressHorizontal.visibility = View.GONE
                Toast.makeText(rootView.context,it.toString(),
                    Toast.LENGTH_SHORT).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val sharedPreferences = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                val params = HashMap<String,String>()
                params["email"] = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    //TO INITIALIZE ALL THE ARRAY LIST USED INT THIS CLASS
    private fun initializeArray() {
        nameArray = ArrayList()
        departmentArray = ArrayList()
        semArray = ArrayList()
        emailArray = ArrayList()
        teamNameArray = ArrayList()
        mobileArray = ArrayList()
    }

    override fun onItemClick(position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
