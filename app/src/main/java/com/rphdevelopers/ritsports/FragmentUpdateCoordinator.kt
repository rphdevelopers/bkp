package com.rphdevelopers.ritsports


import android.accounts.AccountManager
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.fragment_add_coordinator.*
import kotlinx.android.synthetic.main.fragment_coordinators_list.*
import kotlinx.android.synthetic.main.fragment_update_coordinator.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class FragmentUpdateCoordinator : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var const: Const
    val loader = Loader()
    lateinit var rootView: View
    lateinit var spinnerDepartment: Spinner
    lateinit var departmentName: String
    lateinit var uid: String
    lateinit var btnUpdate: Button
    lateinit var etUname: EditText
    lateinit var etUemail: EditText
    lateinit var etMob: EditText
    lateinit var llPassword: LinearLayout
    lateinit var btnPassword: Button
    lateinit var btnChangePassword: Button
    lateinit var etPassword: EditText
    lateinit var etcPassword: EditText
    lateinit var tilcPassword: TextInputLayout
    lateinit var password: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_update_coordinator, container, false)
        btnUpdate = rootView.findViewById(R.id.btn_uc_update)
        etUname = rootView.findViewById(R.id.et_uc_uname)
        etUemail = rootView.findViewById(R.id.et_uc_uemail)
        etMob = rootView.findViewById(R.id.et_uc_umob)
        llPassword = rootView.findViewById(R.id.ll_uc_password)
        btnPassword = rootView.findViewById(R.id.btn_uc_change_password)
        spinnerDepartment = rootView.findViewById(R.id.sp_uc_department)
        etPassword = rootView.findViewById(R.id.et_uc_password)
        etcPassword = rootView.findViewById(R.id.et_uc_cpassword)
        btnChangePassword = rootView.findViewById(R.id.btn_uc_change)
        tilcPassword = rootView.findViewById(R.id.til_uc_cpassword)

        //RETREIVE PASSD DATA FROM SOURCE FRAGMENT
        uid = arguments?.getInt("uid").toString()
        departmentName = arguments?.getString("department").toString()
        const = Const()

        //LOAD PREVIOUS DATA TO TEXT BOX
        loadData(uid)

        //SET PASSWORD CHANGE UI INVISIBLE ON START
        llPassword.visibility = View.GONE

        //UPDATE ONCLICK BUTTON
        btnUpdate.setOnClickListener{
            updateCoordinatorData()
        }

        //spinner item select listener
        spinnerDepartment.onItemSelectedListener = this

        //SHOW AND HIDE CHANGING PASWORD
        btnPassword.setOnClickListener {
            if(llPassword.visibility==View.VISIBLE) {
                llPassword.visibility = LinearLayout.GONE
                var animation = AnimationUtils.loadAnimation(rootView.context, R.anim.layout_scaley_up)
                animation.duration = 500
                llPassword.animation = animation
                llPassword.animate()
                animation.start()
            }else{
                llPassword.visibility = LinearLayout.VISIBLE
                var animation   =    AnimationUtils.loadAnimation(rootView.context, R.anim.layout_scaley)
                animation.duration = 500
                llPassword.animation = animation;
                llPassword.animate()
                animation.start()
            }
        }

        btnChangePassword.setOnClickListener {
            changePassword()
        }
        return rootView
    }

    //TO CHANGE THE PASSWORD - to CALL THE PHP WEB SERVICE
    private fun changePassword() {
        password = etPassword.text.toString()
        val cpassword = etcPassword.text.toString()
        if(password!=cpassword){
            tilcPassword.error = "Password not matching"
        }else{
            doChangePassword()
        }
    }

    //CALL THE WEB SERVICE TO CHANGE THE PASSWORD
    private fun doChangePassword() {
        val stringRequest = object : StringRequest(
            Method.POST, const.PASSWORD_UPDATE_URL,
            Response.Listener<String> { response ->
                try {
                    if (response.trim() { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        Toast.makeText(rootView.context,"Pasword Changed",Toast.LENGTH_SHORT).show()
                        llPassword.visibility = View.GONE
                    } else {
                        Toast.makeText(rootView.context,"Cant Save",Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(rootView.context,"Something went wrong!",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { llProgressBar.visibility = View.GONE }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params.put("uid",uid)
                params.put("password", password)
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        departmentName = spinnerDepartment.getItemAtPosition(position).toString()
    }

    //TO UPDATE DATA WITH PHP
    private fun updateCoordinatorData() {
        val stringRequest = object : StringRequest(
            Method.POST, const.COORDINATOR_UPDATE_URL,
            Response.Listener<String> { response ->
                Toast.makeText(context,response,Toast.LENGTH_SHORT).show()
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        Toast.makeText(rootView.context, "Update Succeccfully",Toast.LENGTH_LONG).show()
                    } else {

                    }
                } catch (e: Exception) {
                    Toast.makeText(rootView.context, "Something went wrong! $e",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { Toast.makeText(rootView.context, "Something went wrong!",Toast.LENGTH_LONG).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["username"] = etUname.text.toString()
                params["useremail"] = etUemail.text.toString()
                params["department"] = departmentName
                params["uid"] = uid
                params["mob"] = etMob.text.toString()
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

    private fun loadData(uid:String) {
        val stringRequest = object : StringRequest(
            Method.POST, const.COORDINATOR_SINGLE_URL,
            Response.Listener<String> { response ->
                var jsonObject = JSONObject(response)
                try {
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("coordinator")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("user_name")
                            val email = jsonObject1.getString("user_email")
                            val mob = jsonObject1.getString("user_mob")
                           // val dep = jsonObject1.getString("dep_name")

                            //TO POPULATE THE DEPARTMENT NAMES
                            loader.setSpinnerDepartment(rootView,spinnerDepartment,departmentName)

                            et_uc_uname.setText(name)
                            et_uc_uemail.setText(checkNull(email))
                            et_uc_umob.setText(checkNull(mob))
                        }
                    }else{
                        Toast.makeText(rootView.context,response,Toast.LENGTH_LONG).show()
                    }
                }catch (e: Exception){}
            },
            Response.ErrorListener {
                Toast.makeText(rootView.context,"Something went wrong...",
                    Toast.LENGTH_SHORT).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["uid"] = uid
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    //chack the value is null or not-null==no data
    private fun checkNull(string: String): String {
        if(string=="null")  return ""
        else return string
    }
}
