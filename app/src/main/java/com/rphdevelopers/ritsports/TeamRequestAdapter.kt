package com.rphdevelopers.ritsports

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.team_request_list.view.*

class TeamRequestAdapter(val semesterArray: ArrayList<String>, val departmentArray: ArrayList<String>,
                         val teamArray: ArrayList<String>, val nameArray: ArrayList<String>,val mobileArray: ArrayList<String>,
                         val emailArray: ArrayList<String>,val context: Context, val lprogress: View, 
                         onItemListener: OnItemListener):
    RecyclerView.Adapter<TeamRequestAdapter.MyViewHolder>(){

    val const = Const()
    var onItemListener: OnItemListener

    init {
        this.onItemListener = onItemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.team_request_list,parent,false)

        return MyViewHolder(view,onItemListener)
    }

    override fun getItemCount() = nameArray.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val sem = semesterArray[position]
        val dep = departmentArray[position]

        holder.department.text = "S$sem - $dep"
        holder.mobile.text = mobileArray[position]
        holder.name.text = nameArray[position]
        holder.teamName.text = teamArray[position]

        holder.btnAccept.setOnClickListener {
            changeStatusNow(emailArray[position],position)
        }

        holder.bntReject.setOnClickListener {
            rejectTeam(emailArray[position],position)
        }
    }

    private fun rejectTeam(email: String, position: Int){
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Reject")
        builder.setMessage("Are you sure...?")
        builder.setPositiveButton("YES"){dialog,which ->
            delete(email, position)
            lprogress.visibility = View.VISIBLE
        }
        builder.setNegativeButton("NO"){dialog,which ->
        }
        var dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun delete(emailid: String, position: Int) {
        val stringRequest = object : StringRequest(
            Method.POST, const.DELETE_TEAM_REQUEST,
            Response.Listener<String> { response ->
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        lprogress.visibility = View.GONE
                        Toast.makeText(context, "Deleted", Toast.LENGTH_LONG).show()
                        removeItemArray(position)
                        notifyDataSetChanged()
                    } else {
                        lprogress.visibility = View.GONE
                        Toast.makeText(context, "Cant delete", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    lprogress.visibility = View.GONE
                    Toast.makeText(context,"Something went wrong!", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                lprogress.visibility = View.GONE
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["email"] = emailid
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    class MyViewHolder(view: View, onItemListener: OnItemListener): RecyclerView.ViewHolder(view),
        View.OnClickListener{
        var onItemListener:OnItemListener

        init {
            view.setOnClickListener(this)
            this.onItemListener = onItemListener
        }
        val department = view.tv_tr_department
        val teamName = view.tv_tr_team_name
        val name = view.tv_tr_name
        val mobile = view.tv_tr_mobile
        val btnAccept = view.btn_tr_accept
        val bntReject = view.btn_tr_reject
        
        override fun onClick(view: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    //TO CHANGE THE STATUS - CALL THE WEBSERVICE
    private fun changeStatusNow(email: String, position: Int){
        val stringRequest = object : StringRequest(
            Method.POST, const.CHANGE_STATUS_URL,
            Response.Listener<String> { response ->
                try {
                    if(response.trim() == const.SUCCESS){
                        removeItemArray(position)
                        notifyDataSetChanged()
                        Toast.makeText(context,"Accepted", Toast.LENGTH_LONG).show()
                    }else{
                        Toast.makeText(context,response, Toast.LENGTH_LONG).show()
                    }
                }catch (e: Exception){
                    Toast.makeText(context,e.toString(), Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                Toast.makeText(context, it.toString(), Toast.LENGTH_LONG).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params[const.USER_EMAIL] = email
                params[const.STATUS] = "1"
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    //TO REMOVE THE REQUEST ITEM FROM DISPLAY
    private fun removeItemArray(position: Int) {
        semesterArray.removeAt(position)
        departmentArray.removeAt(position)
        teamArray.removeAt(position)
        nameArray.removeAt(position)
        mobileArray.removeAt(position)
        emailArray.removeAt(position)
    }

    interface OnItemListener{
        fun onItemClick(position: Int)
    }
}