package com.rphdevelopers.ritsports


import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class FragmentCoordinatorsList : Fragment(),CoordinatorAdapter.OnItemListener {

    //FOR RECYCLER VIEW
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var const: Const
    private lateinit var nameArray: ArrayList<String>
    private lateinit var emailArray: ArrayList<String>
    private lateinit var semArray: ArrayList<String>
    private lateinit var yearArray: ArrayList<String>
    private lateinit var departmentArray: ArrayList<String>
    private lateinit var uidArrayList: ArrayList<String>
    private lateinit var statusArrayList: ArrayList<String>
    private lateinit var rootView: View
    private lateinit var lprogressbar: View
    private lateinit var lprogressHorizontal: View
    private lateinit var department: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_coordinators_list, container, false)
        const = Const()
        lprogressbar = rootView.findViewById<View>(R.id.llProgressBarCoord)
        lprogressHorizontal = rootView.findViewById(R.id.llProgressDelete)

        //FOR RECYCLER VIEW DATA
        viewManager = LinearLayoutManager(rootView.context)
        getcoordinatorData()
        return rootView
    }

    //TO CALL THE WEB SERVICE AND FETH THE COORDINATOR LIST
    private fun getcoordinatorData(){
        lprogressbar.visibility = View.VISIBLE

        //call the webservice
        val stringRequest = object : StringRequest(
            Method.POST, const.COORDINATOR_URL,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        lprogressbar.visibility = View.GONE
                        initializeArray()
                        var jsonArray: JSONArray = jsonObject.getJSONArray("coordinator")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("user_name")
                            department = jsonObject1.getString("dep_name")
                            val sem = jsonObject1.getString("semester")
                            val email = jsonObject1.getString("user_email")
                            val uid = jsonObject1.getString("uid")
                            val status = jsonObject1.getString("status")
                            val year = jsonObject1.getString("academic_year")
                            nameArray.add(name)
                            statusArrayList.add(status)
                            departmentArray.add(department)
                            uidArrayList.add(uid)
                            semArray.add(sem)
                            emailArray.add(email)
                            yearArray.add(year)
                        }
                        viewAdapter = CoordinatorAdapter(nameArray,departmentArray,semArray, emailArray,
                            statusArrayList, yearArray, rootView.context, lprogressHorizontal,this)
                        recyclerView = rootView.findViewById<RecyclerView>(R.id.rvCoordinators).apply{
                            layoutManager = viewManager
                            adapter = viewAdapter
                        }
                        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context,DividerItemDecoration.VERTICAL))
                    }
                } catch (e: Exception) {
                    lprogressbar.visibility = View.GONE
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                lprogressbar.visibility = View.GONE
                Toast.makeText(rootView.context,"Something went wrong...",
                Toast.LENGTH_SHORT).show() }) {
        }
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }


    //TO INITIALIZE ALL THE ARRAY LIST USED INT THIS CLASS
    private fun initializeArray() {
        nameArray = ArrayList()
        departmentArray = ArrayList()
        semArray = ArrayList()
        emailArray = ArrayList()
        uidArrayList = ArrayList()
        statusArrayList = ArrayList()
        yearArray = ArrayList()
    }

    //FOR ITEM CLICK LISTENER
    override fun onItemClick(position: Int) {
        /*var bundle = bundleOf("uid" to uidArrayList[position].toInt(),"from" to const.FROM_CHILD)
        findNavController().navigate(R.id.fragment_account_view, bundle)*/
    }
}