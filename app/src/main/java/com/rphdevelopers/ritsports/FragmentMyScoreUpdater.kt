package com.rphdevelopers.ritsports


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class FragmentMyScoreUpdater : Fragment(), ScoreUpdaterAdapter.OnItemListener {
    lateinit var rootView: View
    lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var nameArray: ArrayList<String>
    private lateinit var emailArray: ArrayList<String>
    private lateinit var progressBar: View
    val const = Const()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_my_score_updater, container, false)
        viewManager = LinearLayoutManager(rootView.context)
        progressBar = rootView.findViewById(R.id.pb_ms_loading)
        emailArray = ArrayList()
        nameArray = ArrayList()

        recyclerView = rootView.findViewById<RecyclerView>(R.id.rv_ms_score_updater).apply{
            layoutManager = LinearLayoutManager(rootView.context)

        }

        showMyScoreUpdater()
        return rootView
    }
    private fun showMyScoreUpdater() {
        progressBar.visibility = View.VISIBLE
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_MY_SCORE_UPDATER_URL,
            Response.Listener<String> { response ->
                progressBar.visibility = View.GONE
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("score_updater")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("user_name")
                            emailArray.add(jsonObject1.getString("user_email"))
                            nameArray.add(name)
                        }
                        viewAdapter = ScoreUpdaterAdapter(nameArray,emailArray,progressBar,this,rootView)
                        recyclerView = rootView.findViewById<RecyclerView>(R.id.rv_ms_score_updater).apply{
                            layoutManager = viewManager
                            adapter = viewAdapter
                        }
                    }
                } catch (e: Exception) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                progressBar.visibility = View.GONE
                Toast.makeText(rootView.context,it.toString(),
                    Toast.LENGTH_SHORT).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val sharedPreferences = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                val params = HashMap<String,String>()
                params["email"] = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()
                return params
            }}
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    override fun onItemClick(position: Int) {

    }


}
