package com.rphdevelopers.ritsports

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.coordinator_list_text_view.view.*

class CoordinatorAdapter(private val name: ArrayList<String>, val department: ArrayList<String>, val semester: ArrayList<String>,
                         val email: ArrayList<String>, val statusArray: ArrayList<String>, val yearArray: ArrayList<String>,
                         val context: Context,val lprogress: View, onItemListener: OnItemListener):
        RecyclerView.Adapter<CoordinatorAdapter.MyViewHolder>(){

    val const = Const()
    var onItemListener: OnItemListener
    val statuses = arrayOf("Active", "Inactive")
    var status = ""

    init {
        this.onItemListener = onItemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.coordinator_list_text_view,parent,false)

        return MyViewHolder(view,onItemListener)
    }

    override fun getItemCount() = name.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val username = if(name[position]=="null") "Name not given" else name[position]
        holder.coordinatorName.text = username
        val sem = semester[position]
        val dep = department[position]
        val year = yearArray[position]
        val status: String = getStatus(statusArray[position])
        holder.coordinatorDepartment.text = "S$sem - $dep($year)"
        holder.coordinatorEmail.text = email[position]
        holder.coordinatorStatus.text = status
        if (status.equals("Active"))
            holder.coordinatorStatus.setTextColor(Color.GREEN)
        else
            holder.coordinatorStatus.setTextColor(Color.RED)
        holder.buttonOption.setOnClickListener{
            val popup = PopupMenu(context,holder.buttonOption)
            popup.inflate(R.menu.coord_menu)
            popup.setOnMenuItemClickListener {
                when(it.itemId){
                    R.id.itm_delete -> deleteCoordinator(holder.coordinatorEmail.text.toString(), position)
                    R.id.itm_change_status -> changeStatus(holder.coordinatorEmail.text.toString(), position)
                    else -> false
                }
            }
            popup.show()
        }
    }

    private fun getStatus(s: String): String {
        if (s.equals("0")){
            return "Inactive"
        }else
            return "Active"
    }

    //TO CHANGE THE STATUS OF THE USER/COORDINATOR
    private fun changeStatus(email: String, position: Int): Boolean {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Change status")
        builder.setSingleChoiceItems(statuses,getStatusPosition(status)){ dialogInterface, i ->
            status = statuses[i]
            changeStatusNow(email, position, status)
            dialogInterface.dismiss()
        }
        builder.setNeutralButton("Cancel"){dialog,which ->
        }
        var dialog: AlertDialog = builder.create()
        dialog.show()
        return  true
    }

    private fun deleteCoordinator(email: String, position: Int): Boolean {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Delete")
        builder.setMessage("Are you want to delete this coordinator.?")
        builder.setPositiveButton("YES"){dialog,which ->
            delete(email, position)
            lprogress.visibility = View.VISIBLE
        }
        builder.setNegativeButton("NO"){dialog,which ->
            Toast.makeText(context, "Not deleted", Toast.LENGTH_LONG).show()
        }
        var dialog: AlertDialog = builder.create()
        dialog.show()
        return true
    }

    private fun delete(emailid: String, position: Int) {
        val stringRequest = object : StringRequest(
            Method.POST, const.DELETE_USER_URL,
            Response.Listener<String> { response ->
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        lprogress.visibility = View.GONE
                        Toast.makeText(context, "Deleted", Toast.LENGTH_LONG).show()
                        department.removeAt(position)
                        semester.removeAt(position)
                        name.removeAt(position)
                        email.removeAt(position)
                        notifyDataSetChanged()
                    } else {
                        lprogress.visibility = View.GONE
                        Toast.makeText(context, "Cant delete", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    lprogress.visibility = View.GONE
                    Toast.makeText(context,"Something went wrong!",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                lprogress.visibility = View.GONE
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params.put(const.USER_EMAIL, emailid)
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    class MyViewHolder(view: View, onItemListener: OnItemListener): RecyclerView.ViewHolder(view),View.OnClickListener{
        var onItemListener:OnItemListener

        init {
            view.setOnClickListener(this)
            this.onItemListener = onItemListener
        }
        val coordinatorName = view.tv_coordinator_name
        val coordinatorEmail = view.tv_coordinator_email
        val coordinatorDepartment = view.tv_coordinator_department
        val buttonOption = view.iv_option
        val coordinatorStatus = view.tv_coordinator_status

        override fun onClick(view: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    //TO CHANGE THE STATUS - CALL THE WEBSERVICE
    private fun changeStatusNow(email: String, position: Int, st: String){
        val stringRequest = object : StringRequest(
            Method.POST, const.CHANGE_STATUS_URL,
            Response.Listener<String> { response ->
                try {
                    if(response.trim().equals(const.SUCCESS)){
                        statusArray[position] = getStatusValue(st)
                        notifyDataSetChanged()
                        Toast.makeText(context,"Status Changed", Toast.LENGTH_LONG).show()

                    }else{
                        Toast.makeText(context,response,Toast.LENGTH_LONG).show()

                    }
                }catch (e: Exception){
                    Toast.makeText(context,e.toString(),Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                Toast.makeText(context, it.toString(), Toast.LENGTH_LONG).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params[const.USER_EMAIL] = email
                params[const.STATUS] = getStatusValue(st)
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    interface OnItemListener{
        fun onItemClick(position: Int)
    }

    //to get the position of status list(active=1 inactive=2)
    private fun getStatusPosition(st: String): Int{
        return if (st.equals("Active")) 1 else 2
    }

    private fun getStatusValue(st: String): String {
        return if (st.equals("Active")) "1" else "0"
    }
}
