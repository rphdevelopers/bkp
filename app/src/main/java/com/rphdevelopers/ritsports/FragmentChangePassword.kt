package com.rphdevelopers.ritsports


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_sign_in.*

/**
 * A simple [Fragment] subclass.
 */
class FragmentChangePassword : Fragment() {

    lateinit var etCuPassword: EditText
    lateinit var etNewPassword: EditText
    lateinit var etCpassword: EditText
    lateinit var btnOk: Button
    lateinit var tilcPassword: TextInputLayout
    lateinit var rootView: View
    lateinit var progressBar: View

    lateinit var sharedPreferences: SharedPreferences
    lateinit var password: String
    lateinit var currentPassword: String
    val const = Const()
    val display = Display()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_change_password, container, false)

        etCpassword = rootView.findViewById(R.id.et_cp_cpassword)
        etCuPassword = rootView.findViewById(R.id.et_cp_currentp)
        etNewPassword = rootView.findViewById(R.id.et_cp_npassord)
        btnOk = rootView.findViewById(R.id.btn_cp_change)
        tilcPassword = rootView.findViewById(R.id.til_cp_cpassword)
        progressBar = rootView.findViewById(R.id.pb_cp_wait)

        sharedPreferences = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)

        btnOk.setOnClickListener {
            password = etNewPassword.text.toString()
            currentPassword = etCuPassword.text.toString()
            val cpassword = etCpassword.text.toString()
            if(currentPassword.equals(""))
                rootView.findViewById<TextInputLayout>(R.id.til_cp_currentp).error = "Current password required"
            if(password.equals(""))
                rootView.findViewById<TextInputLayout>(R.id.til_cp_newpassword).error = "Password required"
            if(password!=cpassword){
                tilcPassword.error = "Password not matching"
            }else{
                doChangePassword()
            }
        }
        return rootView
    }

    //CALL THE WEB SERVICE TO CHANGE THE PASSWORD
    private fun doChangePassword() {
        progressBar.visibility = View.VISIBLE
        val stringRequest = object : StringRequest(
            Method.POST, const.PASSWORD_UPDATE_URL,
            Response.Listener<String> { response ->
                progressBar.visibility = View.GONE
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        display.showSuccessToast(rootView,"Password changed", Gravity.BOTTOM,0,200)
                        findNavController().navigate(R.id.fragment_home)
                    } else {
                        display.showErrorToast(rootView,response, Gravity.BOTTOM,0,200)
                    }
                } catch (e: Exception) {
                    display.showErrorToast(rootView,"Something went wrong!!", Gravity.BOTTOM,0,200)
                }
            },
            Response.ErrorListener {
                progressBar.visibility = View.GONE
                Toast.makeText(rootView.context, it.toString(),Toast.LENGTH_LONG).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["cpassword"] = currentPassword
                params["email"] = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()
                params["password"] = password
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }
}
