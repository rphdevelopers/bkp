package com.rphdevelopers.ritsports

import android.graphics.Color
import kotlinx.android.synthetic.main.players_list.view.*

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.score_updater_list.view.*


class ScoreUpdaterAdapter(private val name: ArrayList<String>,val emailArray: ArrayList<String>, val lprogress: View, onItemListener: OnItemListener, val rootView: View):
    RecyclerView.Adapter<ScoreUpdaterAdapter.MyViewHolder>(){


    val const = Const()
    var onItemListener: OnItemListener

    init {
        this.onItemListener = onItemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.score_updater_list,parent,false)

        return MyViewHolder(view,onItemListener)
    }

    override fun getItemCount() = name.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvName.text = name[position]
        holder.tvEmail.text = emailArray[position]
        if (position%2==0) holder.layout.setBackgroundColor(Color.parseColor("#E719691B"))
        else holder.layout.setBackgroundColor(Color.parseColor("#E70C420E"))
        holder.btnMore.setOnClickListener{
            val popup = PopupMenu(rootView.context,holder.btnMore)
            popup.inflate(R.menu.updater_menu)
            popup.setOnMenuItemClickListener {
                when(it.itemId){
                    R.id.itm_su_delete -> deleteUpdater(holder.tvEmail.text.toString(), position)
                     else -> false
                }
            }
            popup.show()
        }
    }

    private fun deleteUpdater(email: String, position: Int): Boolean {
        val builder = AlertDialog.Builder(rootView.context)
        builder.setTitle("Delete")
        builder.setMessage("Are you want to delete this score updater.?")
        builder.setPositiveButton("YES"){dialog,which ->
            delete(email, position)
            lprogress.visibility = View.VISIBLE
        }
        builder.setNegativeButton("NO"){dialog,which ->
            Toast.makeText(rootView.context, "Not deleted", Toast.LENGTH_LONG).show()
        }
        var dialog: AlertDialog = builder.create()
        dialog.show()
        return true
    }

    private fun delete(email: String, position: Int) {
        val stringRequest = object : StringRequest(
            Method.POST, const.DELETE_SCORE_UPDATER_URL,
            Response.Listener<String> { response ->
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        lprogress.visibility = View.GONE
                        Toast.makeText(rootView.context, "Deleted", Toast.LENGTH_LONG).show()
                        name.removeAt(position)
                        emailArray.removeAt(position)
                        notifyDataSetChanged()
                    } else {
                        lprogress.visibility = View.GONE
                        Toast.makeText(rootView.context, "Cant delete", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    lprogress.visibility = View.GONE
                    Toast.makeText(rootView.context,"Something went wrong!",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                lprogress.visibility = View.GONE
                Toast.makeText(rootView.context, error.toString(), Toast.LENGTH_LONG).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["email"] = email
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

    class MyViewHolder(view: View, onItemListener: OnItemListener): RecyclerView.ViewHolder(view),View.OnClickListener{
        var onItemListener:OnItemListener

        init {
            view.setOnClickListener(this)
            this.onItemListener = onItemListener
        }
        val tvName: TextView = view.tv_sl_updater
        val layout: View = view.ll_sl_updater
        val tvEmail: TextView = view.tv_sl_email
        val btnMore: ImageView = view.iv_sl_more

        override fun onClick(view: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    interface OnItemListener{
        fun onItemClick(position: Int)
    }
}
