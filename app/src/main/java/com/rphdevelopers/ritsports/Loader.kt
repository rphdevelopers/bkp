package com.rphdevelopers.ritsports

import android.R
import android.os.AsyncTask
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL

class Loader{
    val const = Const()
    var departments = ArrayList<String>()

    fun setSpinnerDepartment(rootView: View, spinner: Spinner,departmentName: String){
        val stringRequest = object : StringRequest(
            Method.POST, const.DEPARTMENT_URL,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("department")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("dep_name")
                            departments.add(name)
                        }
                    }
                    var adapter = ArrayAdapter<String>(
                        rootView.context,
                        R.layout.simple_spinner_dropdown_item,
                        departments
                    )
                    spinner.adapter = adapter
                    spinner.setSelection(adapter.getPosition(departmentName))
                } catch (e: Exception) {
                    Toast.makeText(rootView.context, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { Toast.makeText(rootView.context,"Something went wrong...",
                Toast.LENGTH_SHORT).show() }) {
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

    fun setSingleSpinner(rootView: View, spinner: Spinner,sqlUrl: String, arrayName: String, colname: String){
        var arrayList = ArrayList<String>()
        val stringRequest = object : StringRequest(
            Method.POST, sqlUrl,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray(arrayName)
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString(colname)
                            arrayList.add(name)
                        }
                    }
                    var adapter = ArrayAdapter<String>(
                        rootView.context,
                        R.layout.simple_spinner_dropdown_item,
                        arrayList
                    )
                    spinner.setAdapter(adapter)
                } catch (e: Exception) {
                    Toast.makeText(rootView.context, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { Toast.makeText(rootView.context,"Something went wrong...",
                Toast.LENGTH_SHORT).show() }) {
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

}