package com.rphdevelopers.ritsports

import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class VolleyRequests {
    val const = Const()
    val display = Display()

    fun insert(url: String, rootView: View,params: HashMap<String,String>,success: String){
        val stringRequest = object : StringRequest(
            Method.POST, url,
            Response.Listener<String> { response ->
                try {
                    if(response.trim().equals(const.SUCCESS)){
                        display.showSuccessToast(rootView,success, Gravity.BOTTOM,0,200)

                    }else{
                        display.showErrorToast(rootView,response, Gravity.BOTTOM,0,200)

                    }
                }catch (e: Exception){
                    Toast.makeText(rootView.context,e.toString(), Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                Toast.makeText(rootView.context, it.toString(), Toast.LENGTH_LONG).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }
}