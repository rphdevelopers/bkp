package com.rphdevelopers.ritsports


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * A simple [Fragment] subclass.
 */
class FragmentAddTournament : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var rootView: View
    lateinit var btnSdate: Button
    lateinit var btnEdate: Button
    lateinit var btnAdd: Button
    lateinit var etTname: EditText
    lateinit var spStype: Spinner
    lateinit var spFtype: Spinner
    lateinit var spPcount: Spinner
    lateinit var llPcount: LinearLayout
    lateinit var tilName: TextInputLayout
    lateinit var tilSdate: TextInputLayout
    lateinit var tilEdate: TextInputLayout
    lateinit var tilFee: TextInputLayout
    lateinit var tilMperday: TextInputLayout
    lateinit var etFee: EditText
    lateinit var etMperday: EditText
    lateinit var sharedPrefs: SharedPreferences
    var nplayerArray = ArrayList<String>()

    val loader = Loader()
    val const = Const()
    val volleyRequests = VolleyRequests()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_add_tournament, container, false)
        btnSdate = rootView.findViewById(R.id.btn_at_sdate)
        btnEdate = rootView.findViewById(R.id.btn_at_edate)
        btnAdd = rootView.findViewById(R.id.btn_at_add_tour)
        etTname = rootView.findViewById(R.id.et_at_name)
        spFtype = rootView.findViewById(R.id.sp_at_ftype)
        spPcount = rootView.findViewById(R.id.sp_at_pcount)
        spStype = rootView.findViewById(R.id.sp_at_type)
        llPcount = rootView.findViewById(R.id.ll_at_nplayers)
        tilName = rootView.findViewById(R.id.til_at_name)
        tilSdate = rootView.findViewById(R.id.til_at_sdate)
        tilEdate = rootView.findViewById(R.id.til_at_edate)
        etMperday = rootView.findViewById(R.id.et_at_mperday)
        etFee = rootView.findViewById(R.id.et_at_fee)
        tilMperday = rootView.findViewById(R.id.til_at_mperday)
        tilFee = rootView.findViewById(R.id.til_at_fee)

        sharedPrefs = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)

        nplayerArray.add("3")
        nplayerArray.add("7")
        nplayerArray.add("9")

        //TO LOAD THE SPORTS SPINNER ITEMS
        loader.setSingleSpinner(rootView,spStype,const.GET_SPORTS_URL,"sports","spname")
        loader.setSingleSpinner(rootView,spFtype,const.GET_FIXTURE_URL,"fixture","fix_type")
        spStype.onItemSelectedListener = this


        btnSdate.setOnClickListener {
            showDatePickerDialog(btnSdate)
        }
        btnEdate.setOnClickListener {
            showDatePickerDialog(btnEdate)
        }

        //TO SAVE THE TOURNAMENT
        btnAdd.setOnClickListener {
            val name = etTname.text.toString()
            val stype = spStype.selectedItem.toString()
            var numPlayers = "0"
            if(stype=="Cricket")
                numPlayers = "11"
            else
                numPlayers = spPcount.selectedItem.toString()
            val fixType = spFtype.selectedItem.toString()
            val sdate = btnSdate.text.toString()
            val edate = btnEdate.text.toString()
            if(name.isEmpty())
                tilName.error = "Name required"
            else if(sdate==resources.getString(R.string.start_date))
                tilSdate.error = "Date needed"
            else if(edate==resources.getString(R.string.end_date))
                tilEdate.error = "Date needed"
            else if(etMperday.text.toString().isEmpty())
                tilMperday.error = "Match per day required"
            else if(etFee.text.toString().isEmpty())
                tilFee.error = "Fee is required"
            else{
                val params = HashMap<String,String>()
                params["email"] = sharedPrefs.getString(const.EMAIL_SHARED_PREF, "").toString()
                params["sp_name"] = spStype.selectedItem.toString()
                params["fix_name"] = spFtype.selectedItem.toString()
                params["tou_name"] = etTname.text.toString()
                params["mper_day"] = etMperday.text.toString()
                params["s_date"] = convertDate(btnSdate.text.toString())
                params["e_date"] = convertDate(btnEdate.text.toString())
                params["fee"] = etFee.text.toString()
                if(stype=="Cricket")
                    params["p_count"] = "11"
                else
                    params["p_count"] = spPcount.selectedItem.toString()


                volleyRequests.insert(const.ADD_TOURNAMENT_URL,rootView,params,"Tournament Added")
            }

        }

        return rootView
    }

    private fun convertDate(date: String): String {
        var newDate: String
        val initDate = SimpleDateFormat("dd/MM/yyyy").parse(date)
        newDate = SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).format(initDate)
        return newDate
    }

    //TO SHOW DATE PICKER
    fun showDatePickerDialog(btn: Button) {
        val fm = (activity as AppCompatActivity).supportFragmentManager
        val newFragment = Display.DatePickerFragment(rootView,btn)
        newFragment.show(fm, "datePicker")
    }

    //SPINNER ON ITEM SELECTED LISTENER
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spId: String = parent?.id.toString()
        if (spId.toInt() == R.id.sp_at_type) {
            val sportType = spStype.getItemAtPosition(position).toString()
            if(sportType=="Cricket") {
                llPcount.visibility = View.GONE
            }
            else {
                llPcount.visibility = View.VISIBLE
                setPlayerCountSpinner()
            }
        }
    }

    private fun setPlayerCountSpinner() {
        var adapter = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            nplayerArray
        )
        spPcount.adapter = adapter
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
