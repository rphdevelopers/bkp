package com.rphdevelopers.ritsports

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentAddCoordinator : Fragment(), AdapterView.OnItemSelectedListener {
    // TODO: Rename and change types of parameters
    val loader = Loader()
    val validator = Validator()
    lateinit var btnAddCoordinator: Button
    lateinit var etUEmail: EditText
    lateinit var spDepartment: Spinner
    lateinit var spSemester: Spinner
    lateinit var spYear: Spinner
    lateinit var const: Const
    lateinit var rootView: View
    lateinit var department: String
    lateinit var etPassword: EditText
    lateinit var useremail:String
    lateinit var tilEmail: TextInputLayout
    lateinit var tilPassword: TextInputLayout
    lateinit var btnGeneratePassword: Button
    lateinit var password: String
    lateinit var semester: String
    lateinit var academicYear: String

    var departments = ArrayList<String>()
    var yearArray = ArrayList<String>()
    var semEight = ArrayList<String>()
    var semSix = ArrayList<String>()
    var semTen = ArrayList<String>()
    var semFour = ArrayList<String>()
    var sem = ArrayList<String>()
    val display = Display()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_coordinator, container, false)

        btnAddCoordinator = rootView.findViewById(R.id.btn_add_coordinator)
        spDepartment = rootView.findViewById(R.id.spDepartment)
        spYear = rootView.findViewById(R.id.spYear)
        spSemester = rootView.findViewById(R.id.spSemester)
        etUEmail = rootView.findViewById(R.id.et_ac_email)
        etPassword = rootView.findViewById(R.id.et_ac_password)
        tilEmail = rootView.findViewById(R.id.til_ac_email)
        tilPassword = rootView.findViewById(R.id.til_ac_password)
        btnGeneratePassword = rootView.findViewById(R.id.btn_generate_password)
        const = Const()
        semester = "1"
        password = ""
        academicYear = "121"


        loadSetSem()

        //TO POPULATE DEPARTMENTS
        loader.setSpinnerDepartment(rootView,spDepartment,"MCA")

        btnAddCoordinator.setOnClickListener {
            val email = etUEmail.text.toString()
            password = etPassword.text.toString()
            if(password.isEmpty())
                tilPassword.error = "Password required"
            if(email.isEmpty())
                tilEmail.error = "Email required"
            else if(validator.isEmailValid(email))
                addCoordinator()
            else
                tilEmail.error = "Invalid email address"
        }

        btnGeneratePassword.setOnClickListener {
            var myPasswordManager = PasswordManager()
            password = myPasswordManager.generatePassword(true,true,true,false,6)
            etPassword.setText(password)
        }

        spDepartment.onItemSelectedListener = this
        spYear.onItemSelectedListener = this
        return rootView;
    }

    //TO ADD A NEW COORDINATOR USING WEB SERVICE
    //REGISTER NEW COORDINATOR
    private fun addCoordinator() {
        useremail = etUEmail.text.toString()
        doRequest(const.ADD_COORDINATOR_URL)
    }


    //SPINNER SELECTED LISTENER
    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spId:  String = parent?.id.toString()
        var duration: Int
        if (spId.toInt() == R.id.spDepartment){
            department = spDepartment.getItemAtPosition(position).toString()
            if(department=="MCA"){
                sem = semSix
                duration = 3
            }else if(department=="BArch"){
                sem = semTen
                duration = 5
            }else if(department=="MTech"){
                sem = semFour
                duration = 2
            }else{
                sem = semEight
                duration = 4
            }
            var adapter = ArrayAdapter<String>(
                rootView.context,
                android.R.layout.simple_spinner_dropdown_item,
                sem
            )
            spSemester.adapter = adapter
            var adapterYear = ArrayAdapter<String>(
                rootView.context,
                android.R.layout.simple_spinner_dropdown_item,
                loadAcademicYear(duration)
            )
            spYear.adapter = adapterYear
        }else if(spId.toInt().equals(R.id.spSemester)){
            semester = spSemester.getItemAtPosition(position).toString()
        }else if(spId.toInt().equals(R.id.spYear)){
            academicYear = spYear.getItemAtPosition(position).toString()
            Toast.makeText(rootView.context,academicYear, Toast.LENGTH_LONG).show()
        }

    }

    //TO CAL THE ADD TORetUEmailNT WEB SERVICE
    private fun doRequest(url: String){
        val stringRequest = object : StringRequest(
            Method.POST, url,
            Response.Listener<String> { response ->
                try {
                    if(response.trim().equals(const.SUCCESS)){
                        val intent = Intent(Intent.ACTION_SENDTO).apply {
                            data = Uri.parse("mailto:${etUEmail.text.toString()}")
                         //   putExtra(Intent.EXTRA_EMAIL,arrayOf({etUEmail.text.toString()}))
                            putExtra(Intent.EXTRA_SUBJECT,"RITSports coordinator registration")
                            putExtra(Intent.EXTRA_TEXT,"Hello\n\t\tYour password for the RITSports application is : " +
                                    "${etPassword.text.toString()}")
                        }
                        startActivity(intent)

                        etPassword.setText("")
                        etUEmail.setText("")
                        display.showSuccessToast(rootView,"Added",Gravity.BOTTOM,0,200)

                    }else{
                        display.showErrorToast(rootView,response,Gravity.BOTTOM,0,200)

                    }
                }catch (e: Exception){
                    Toast.makeText(rootView.context,e.toString(),Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                Toast.makeText(rootView.context, it.toString(), Toast.LENGTH_LONG).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params[const.DEPARTMENT] = department
                params[const.USER_EMAIL] = useremail
                params[const.PASSWORD] = password
                params[const.SEMESTER] = semester
                params[const.ACADEMIC_YEAR] = academicYear
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    //TO LOAD THE SEMESTER SPINNER
    private fun loadSetSem(){
        for(i in 1..4){
            semFour.add(i.toString())
        }
        for(i in 1..6){
            semSix.add(i.toString())
        }
        for(i in 1..8){
            semEight.add(i.toString())
        }
        for(i in 1..10){
            semTen.add(i.toString())
        }
    }

    //TO LOAD TE YEAR SPINNER
    private fun loadAcademicYear(duration: Int): ArrayList<String> {
        val year: Int = Calendar.getInstance().get(Calendar.YEAR)
        var array = ArrayList<String>()
        for(i in (year-duration)..(year)){
            var d: Int = i+duration
            array.add("$i-$d")
        }
        return array
    }
}
