package com.rphdevelopers.ritsports


import android.accounts.AccountManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.fragment_add_players.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class FragmentAddPlayers : Fragment() {

    lateinit var etp1: EditText
    lateinit var etp2: EditText
    lateinit var etp3: EditText
    lateinit var etp4: EditText
    lateinit var etp5: EditText
    lateinit var etp6: EditText
    lateinit var etp7: EditText
    lateinit var etp8: EditText
    lateinit var etp9: EditText
    lateinit var etp10: EditText
    lateinit var etp11: EditText
    lateinit var ets1: EditText
    lateinit var ets2: EditText
    lateinit var ets3: EditText
    lateinit var btnAdd: Button
    lateinit var btnUpdate: Button
    lateinit var rootView: View
    lateinit var prefs: SharedPreferences
    lateinit var progressBar: View
    lateinit var navView: NavigationView

    var nameArray = ArrayList<String>()
    var idArray = ArrayList<String>()

    var params = HashMap<String,String>()
    var tid = "0"
    var pcount = "0"
    val const = Const()
    val volleyRequests = VolleyRequests()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_players, container, false)
        navView = activity!!.findViewById(R.id.nav_view)
        initializeObjects()

        //ACTIVITY TO UPDATE OR ADD NEW PLAYER INFORMATION
        if(prefs.getBoolean(const.IS_PLAYER_ADDED_SHARED_PREF,false)) {
            btnAdd.visibility = View.GONE
            btnUpdate.visibility = View.VISIBLE
        }else{
            btnAdd.visibility = View.VISIBLE
            btnUpdate.visibility = View.GONE
        }
        //GET TEAM ID AND PLAYER COUNT
        getData()

        //button add players onclick listener
        btnAdd.setOnClickListener {
            params["count"] = pcount
            params["tid"]=tid
            when(pcount){
                "3" -> {
                    if(isEmptyOrNot(3)){
                        setParams3()
                        params["s1"] = ets1.text.toString()
                        params["s2"] = ets2.text.toString()
                        volleyRequests.insert(const.ADD_PLAYERS_URL,rootView,params,"Player details added")
                    }
                }
                "7" -> {
                    if(isEmptyOrNot(7)){
                        setParams7()
                        setParamsSub()
                        volleyRequests.insert(const.ADD_PLAYERS_URL,rootView,params,"Player details added")
                    }
                }
                "9" -> {
                    if(isEmptyOrNot(9)){
                        setParams9()
                        setParamsSub()
                        volleyRequests.insert(const.ADD_PLAYERS_URL,rootView,params,"Player details added")
                    }
                }
                "11" -> {
                    if(isEmptyOrNot(11)){
                        setParams11()
                        setParamsSub()
                        volleyRequests.insert(const.ADD_PLAYERS_URL,rootView,params,"Player details added")
                    }
                }
            }
            navView.menu.clear()
            navView.inflateMenu(R.menu.nav_team_manager_update_menu)
        }

        //button update players onclick
        btnUpdate.setOnClickListener {

        }

        //TO CLEAR TIL ERROR
        clearTheTextInputError()

        return rootView
    }

    private fun setParams11() {
        setParams9()
        params["p10"] = etp10.text.toString()
        params["p11"] = etp11.text.toString()
    }
    private fun setParams9() {
        setParams7()
        params["p8"] = etp8.text.toString()
        params["p9"] = etp9.text.toString()
    }
    private fun setParams7() {
        setParams3()
        params["p4"] = etp4.text.toString()
        params["p5"] = etp5.text.toString()
        params["p6"] = etp6.text.toString()
        params["p7"] = etp7.text.toString()
    }
    //TO SET THE PARAMETER player names
    private fun setParams3() {
        params["p1"] = etp1.text.toString()
        params["p2"] = etp2.text.toString()
        params["p3"] = etp3.text.toString()
    }

    //TO CLEAR THE ERROR IN TEXT INPUT
    private fun clearTheTextInputError() {
        etp1.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p1.error = ""
            }

        })
        etp2.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p2.error = ""
            }

        })
        etp3.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p3.error = ""
            }

        })
        etp4.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p4.error = ""
            }

        })
        etp5.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p5.error = ""
            }

        })
        etp6.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p6.error = ""
            }

        })
        etp7.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p7.error = ""
            }

        })
        etp8.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p8.error = ""
            }

        })
        etp9.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p9.error = ""
            }

        })
        etp10.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p10.error = ""
            }

        })
        etp11.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_p11.error = ""
            }

        })
        ets1.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_s1.error = ""
            }

        })
        ets2.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_s2.error = ""
            }

        })
        ets3.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                til_ap_s3.error = ""
            }

        })
    }

    //TO SET SUBSTITUTES
    private fun setParamsSub() {
        params["s1"] = ets1.text.toString()
        params["s2"] = ets2.text.toString()
        params["s3"] = ets3.text.toString()
    }

    //GET TEAM ID AND PLAYER COUNT
    private fun getData() {
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_TEAM_ID,
            Response.Listener<String> { response ->
                try {
                    val jsonObject: JSONObject = JSONObject(response)
                    tid = jsonObject.getString("teamid")
                    pcount = jsonObject.getString("pcount")

                    //SET PLAYER COUNT
                    setPlayerCount()
                    //UPDATE OR ADD PLAYER
                    if(prefs.getBoolean(const.IS_PLAYER_ADDED_SHARED_PREF,false)) {
                        setUpdatePlayers()
                    }

                } catch (e: Exception) {
                    Toast.makeText(rootView.context,e.toString(), Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { llProgressBar.visibility = View.GONE }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val sharedPreferences = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME,Context.MODE_PRIVATE)
                val prams = HashMap<String,String>()
                val KEY_EMAIL = "email"
                val email = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()
                prams[KEY_EMAIL] = email
                return prams
            }
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

    //TO GET AND DISPLAY THE PLAYER NAMES FOR UPDATING PROCESS
    private fun setUpdatePlayers() {
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_MY_PLAYERS_URL,
            Response.Listener<String> { response ->
                progressBar.visibility = View.GONE
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("players")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("pl_name")
                            idArray.add(jsonObject1.getString("player_id"))
                            nameArray.add(name)
                        }
                        displayPlayers()
                    }
                } catch (e: Exception) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                progressBar.visibility = View.GONE
                Toast.makeText(rootView.context,it.toString(),
                    Toast.LENGTH_SHORT).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val sharedPreferences = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                val params = HashMap<String,String>()
                params["email"] = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()
                return params
            }}
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    //TO DISPLAY THE ALREADY ADDED PLAYER NAMES
    private fun displayPlayers() {
        var et = ArrayList<EditText>()
        et.add(etp1)
        et.add(etp2)
        et.add(etp3)
        et.add(etp4)
        et.add(etp5)
        et.add(etp6)
        et.add(etp7)
        et.add(etp8)
        et.add(etp9)
        et.add(etp10)
        et.add(etp11)

        for(i in 0..pcount.toInt()){
            et[i].setText(nameArray[i])
        }
        ets1.setText(nameArray[pcount.toInt()])
        ets2.setText(nameArray[pcount.toInt()+1])
        if(!pcount.toInt().equals(3)){
            ets3.setText(nameArray[pcount.toInt()+2])
        }
    }

    //TO SHOW EDIT TEXT BASED ON PLAYER COUNT 3S 7S 9S 11S
    private fun setPlayerCount() {
        ll_ap_seven.visibility = View.VISIBLE
        ll_ap_nine.visibility = View.VISIBLE
        ll_ap_eleven.visibility = View.VISIBLE
        ets3.visibility = View.VISIBLE
        when(pcount){
            "3" -> {
                ll_ap_seven.visibility = View.GONE
                ll_ap_eleven.visibility = View.GONE
                ll_ap_nine.visibility = View.GONE
                ets3.visibility = View.GONE
            }
            "7" -> {
                ll_ap_eleven.visibility = View.GONE
                ll_ap_nine.visibility = View.GONE
            }
            "9" -> {
                ll_ap_eleven.visibility = View.GONE
            }
        }
    }

    //TO INITIALIZE THE OBJECTS
    private fun initializeObjects() {
        etp1 = rootView.findViewById(R.id.et_ap_p1)
        etp2 = rootView.findViewById(R.id.et_ap_p2)
        etp3 = rootView.findViewById(R.id.et_ap_p3)
        etp4 = rootView.findViewById(R.id.et_ap_p4)
        etp5 = rootView.findViewById(R.id.et_ap_p5)
        etp6 = rootView.findViewById(R.id.et_ap_p6)
        etp7 = rootView.findViewById(R.id.et_ap_p7)
        etp8 = rootView.findViewById(R.id.et_ap_p8)
        etp9 = rootView.findViewById(R.id.et_ap_p9)
        etp10 = rootView.findViewById(R.id.et_ap_p10)
        etp11 = rootView.findViewById(R.id.et_ap_p11)
        ets1 = rootView.findViewById(R.id.et_ap_s1)
        ets2 = rootView.findViewById(R.id.et_ap_s2)
        ets3 = rootView.findViewById(R.id.et_ap_s3)

        btnAdd = rootView.findViewById(R.id.btn_ap_add)
        btnUpdate = rootView.findViewById(R.id.btn_ap_update)
        progressBar = rootView.findViewById(R.id.pb_ap_loading)
        prefs = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME,Context.MODE_PRIVATE)
    }

    // ##TO CHECK is empty or not
    fun isEmptyOrNot(n: Int): Boolean{
        var flag = true
        when (n){
            3 -> flag = check3(flag)
            7 -> flag = check7(check3(flag))
            9 -> flag = check9(check7(check3(flag)))
            11 -> flag = (check11(check9(check7(check3(flag)))))
        }
        if(ets1.text.toString().isEmpty()){
            flag = false
            til_ap_s1.error = "Required"
        }
        if(ets2.text.toString().isEmpty()){
            flag = false
            til_ap_s2.error = "Required"
        }
        if(n!=3){
            if(ets3.text.toString().isEmpty()){
                flag = false
                til_ap_s3.error = "Required"
            }
        }

        return flag
    }
    private fun check11(flag: Boolean): Boolean {
        var result = flag
        if(etp10.text.toString().isEmpty()){
            result = false
            til_ap_p10.error = "Required"
        }
        if(etp11.text.toString().isEmpty()){
            result = false
            til_ap_p11.error = "Required"
        }
        return result
    }
    private fun check9(flag: Boolean): Boolean {
        var result = flag
        if(etp8.text.toString().isEmpty()){
            result = false
            til_ap_p8.error = "Required"
        }
        if(etp9.text.toString().isEmpty()){
            result = false
            til_ap_p9.error = "Required"
        }
        if(etp9.text.toString().isEmpty()){
            result = false
            til_ap_p9.error = "Required"
        }
        return result
    }
    private fun check7(flag: Boolean): Boolean {
        var result = flag
        if(etp4.text.toString().isEmpty()){
            result = false
            til_ap_p4.error = "Required"
        }
        if(et_ap_p5.text.toString().isEmpty()){
            result = false
            til_ap_p5.error = "Required"
        }
        if(etp6.text.toString().isEmpty()){
            result = false
            til_ap_p6.error = "Required"
        }
        if(etp7.text.toString().isEmpty()){
            result = false
            til_ap_p7.error = "Required"
        }
        return result
    }
    private fun check3(flag: Boolean): Boolean {
        var result = flag
        if(etp1.text.toString().isEmpty()){
            result = false
            til_ap_p1.error = "Required"
        }
        if(etp2.text.toString().isEmpty()){
            result = false
            til_ap_p2.error = "Required"
        }
        if(etp3.text.toString().isEmpty()){
            result = false
            til_ap_p3.error = "Required"
        }
        return result
    }
    //#end
}
