package com.rphdevelopers.ritsports


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_register_team.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * A simple [Fragment] subclass.
 */
class FragmentRegisterTeam : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var etName: EditText
    lateinit var etEmail: EditText
    lateinit var etMobile: EditText
    lateinit var etPassword: EditText
    lateinit var etCpassword: EditText
    lateinit var spDepartment: Spinner
    lateinit var spTournament: Spinner
    lateinit var spSemester: Spinner
    lateinit var spYear: Spinner
    lateinit var btnRegister: Button
    lateinit var rootView: View
    lateinit var etTname: EditText
    var semEight = ArrayList<String>()
    var semSix = ArrayList<String>()
    var semTen = ArrayList<String>()
    var semFour = ArrayList<String>()
    var sem = ArrayList<String>()
    var tournamentArray = ArrayList<String>()
    val loader = Loader()
    val validator = Validator()
    val volleyRequests = VolleyRequests()
    val const = Const()
    var duration = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_register_team, container, false)

        //INITIALIZE ALL OBJECTS AND VARIABLES
        initializeObjects()
        loadSetSem()
        //POPULATE Department spinner
        loader.setSpinnerDepartment(rootView,spDepartment,"MCA")

        //POPULATE TOURNAMENT LIST
        loadTournamentName()

        spDepartment.onItemSelectedListener = this

        //REGISTER BUTTON CLICK LISTENER
        btnRegister.setOnClickListener {
            registerTeamUser()
        }

        return rootView
    }

    //LOAD THE TOURNAMENT LIST
    private fun loadTournamentName() {
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_TOURNAMENT_URL,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("tournament")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val tName = jsonObject1.getString("tou_name")
                            tournamentArray.add(tName)
                        }
                        var adapter = ArrayAdapter<String>(
                            rootView.context,
                            android.R.layout.simple_spinner_dropdown_item,
                            tournamentArray
                        )
                        spTournament.adapter = adapter
                    }
                } catch (e: Exception) {
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                Toast.makeText(rootView.context,"Something went wrong...",
                    Toast.LENGTH_SHORT).show() }) {
        }
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    //TO REGISTER THE TEAM
    private fun registerTeamUser() {
        if(checkData()){
            val params = getParams()
            volleyRequests.insert(const.REGISTER_TEAM_URL,rootView,params,"Registered\nWait for approval")
        }
    }

    //TO GET THE VALUES TO REGISTER
    private fun getParams(): HashMap<String, String> {
        val params = HashMap<String,String>()
        params["name"] = etName.text.toString()
        params["email"] = etEmail.text.toString()
        params["mobile"] = etMobile.text.toString()
        params["password"] = etPassword.text.toString()
        params["department"] = spDepartment.selectedItem.toString()
        params["semester"] = spSemester.selectedItem.toString()
        params["academic_year"] = spYear.selectedItem.toString()
        params["team_name"] = etTname.text.toString()
        params["tournament"] = spTournament.selectedItem.toString()
        return params
    }

    //VALIDATE THE DATA
    private fun checkData(): Boolean {
        var result = true
        if(etName.text.toString().isEmpty()) {
            til_rt_name.error = "Name required"
            result = false
        }
        if(etEmail.text.toString().isEmpty()){
            til_rt_email.error = "Email required"
            result = false
        }else if (!validator.isEmailValid(etEmail.text.toString())){
            til_rt_email.error = "Not a valid email address"
            result = false
        }
        if (etMobile.text.toString().isEmpty()){
            til_rt_mobile.error = "Mobile number required"
            result = false
        }
        if(etPassword.text.toString().isEmpty()){
            til_rt_password.error = "Enter a password"
            result = false
        }
        if (etPassword.text.toString() != etCpassword.text.toString()){
            til_rt_password.error = "Password not match"
            result = false
        }
        if(etTname.text.toString().isEmpty()){
            til_rt_tname.error = "Enter a name for your team"
            result  = false
        }
        return result
    }

    //INITIALIZE OBJECTS
    private fun initializeObjects() {
        etName = rootView.findViewById(R.id.et_rt_name)
        etEmail = rootView.findViewById(R.id.et_rt_email)
        etMobile = rootView.findViewById(R.id.et_rt_mobile)
        etPassword = rootView.findViewById(R.id.et_rt_password)
        etCpassword = rootView.findViewById(R.id.et_rt_cpassword)
        spDepartment = rootView.findViewById(R.id.sp_rt_department)
        spSemester = rootView.findViewById(R.id.sp_rt_semester)
        spTournament = rootView.findViewById(R.id.sp_rt_tournament)
        btnRegister = rootView.findViewById(R.id.btn_rt_register)
        spYear = rootView.findViewById(R.id.sp_rt_year)
        etTname = rootView.findViewById(R.id.et_rt_tname)
    }

    //TO LOAD THE SEMESTER SPINNER
    private fun loadSetSem(){
        for(i in 1..4){
            semFour.add(i.toString())
        }
        for(i in 1..6){
            semSix.add(i.toString())
        }
        for(i in 1..8){
            semEight.add(i.toString())
        }
        for(i in 1..10){
            semTen.add(i.toString())
        }
    }

    //SPINNER OnItem selected listener
    override fun onNothingSelected(p0: AdapterView<*>?) {
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spId:  String = parent?.id.toString()
        if (spId.toInt() == R.id.sp_rt_department){
            val department = spDepartment.getItemAtPosition(position).toString()
            if(department=="MCA"){
                sem = semSix
                duration = 3
            }else if(department=="BArch"){
                sem = semTen
                duration = 5
            }else if(department=="MTech"){
                sem = semFour
                duration = 2
            }else{
                sem = semEight
                duration = 4
            }
            var adapter = ArrayAdapter<String>(
                rootView.context,
                android.R.layout.simple_spinner_dropdown_item,
                sem
            )
            spSemester.adapter = adapter
            var adapterYear = ArrayAdapter<String>(
                rootView.context,
                android.R.layout.simple_spinner_dropdown_item,
                loadAcademicYear(duration)
            )
            spYear.adapter = adapterYear
        }
    }

    //TO LOAD TE YEAR SPINNER
    private fun loadAcademicYear(duration: Int): ArrayList<String> {
        val year: Int = Calendar.getInstance().get(Calendar.YEAR)
        var array = ArrayList<String>()
        for(i in (year-duration)..(year)){
            var d: Int = i+duration
            array.add("$i-$d")
        }
        return array
    }
}
