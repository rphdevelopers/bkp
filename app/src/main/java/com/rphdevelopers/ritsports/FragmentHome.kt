package com.rphdevelopers.ritsports

import android.os.Bundle
import android.text.Layout
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentHome : Fragment(), TournamentAdapter.OnItemListener {

    lateinit var rootView: View
    lateinit var progressBar: View
    lateinit var rvTournament: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var tidArray: ArrayList<String>
    private lateinit var spNameArray: ArrayList<String>
    private lateinit var tNameArray: ArrayList<String>
    private lateinit var pcountArray: ArrayList<String>
    private lateinit var sdateArray: ArrayList<String>
    private lateinit var edateArray: ArrayList<String>
    private lateinit var coordinatorArray: ArrayList<String>

    val const = Const()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false)
        progressBar = rootView.findViewById(R.id.pb_hm_loading)
        viewManager = LinearLayoutManager(rootView.context)

        showTournaments()
        return rootView
    }

    //TO CALL THE WEB SERVICE AND FETH THE COORDINATOR LIST
    private fun showTournaments(){
        progressBar.visibility = View.VISIBLE

        //call the webservice
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_TOURNAMENT_URL,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        progressBar.visibility = View.GONE
                        initializeArray()
                        var jsonArray: JSONArray = jsonObject.getJSONArray("tournament")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val tid = jsonObject1.getString("tou_id")
                            val spName = jsonObject1.getString("spname")
                            val tName = jsonObject1.getString("tou_name")
                            val pCount = jsonObject1.getString("player_count")
                            val sDate = jsonObject1.getString("start_date")
                            val eDate = jsonObject1.getString("end_date")
                            val coordinator = jsonObject1.getString("dep_name")

                            tidArray.add(tid)
                            spNameArray.add(spName)
                            tNameArray.add(tName)
                            pcountArray.add(pCount)
                            sdateArray.add(sDate)
                            edateArray.add(eDate)
                            coordinatorArray.add(coordinator)
                        }
                        viewAdapter = TournamentAdapter(tidArray,spNameArray,tNameArray, pcountArray,
                            sdateArray, edateArray, coordinatorArray, rootView.context, progressBar,this)
                        rvTournament = rootView.findViewById<RecyclerView>(R.id.rv_hm_tournaments).apply{
                            layoutManager = viewManager
                            adapter = viewAdapter
                        }

                    }
                } catch (e: Exception) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                progressBar.visibility = View.GONE
                Toast.makeText(rootView.context,it.toString(), Toast.LENGTH_SHORT).show()
            }) {
        }
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    private fun initializeArray() {
        tidArray = ArrayList()
        spNameArray = ArrayList()
        tNameArray = ArrayList()
        pcountArray = ArrayList()
        sdateArray = ArrayList()
        edateArray = ArrayList()
        coordinatorArray = ArrayList()

    }

    override fun onItemClick(position: Int) {
        Toast.makeText(rootView.context,tNameArray[position],Toast.LENGTH_LONG).show()
    }

}
