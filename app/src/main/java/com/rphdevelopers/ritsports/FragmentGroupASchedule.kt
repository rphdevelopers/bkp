package com.rphdevelopers.ritsports


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_group_aschedule.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * A simple [Fragment] subclass.
 */
class FragmentGroupASchedule : Fragment(), AdapterView.OnItemSelectedListener,
    View.OnClickListener {

    lateinit var rootView: View
    lateinit var teamidArray: ArrayList<String>
    lateinit var depArray: ArrayList<String>
    lateinit var semArray: ArrayList<String>
    lateinit var teamArray: ArrayList<String>
    lateinit var spinnerArray: ArrayList<Spinner>
    lateinit var adapterArray: ArrayList<ArrayAdapter<String>>
    lateinit var buttonArray: ArrayList<Button>
    lateinit var prefs: SharedPreferences
    lateinit var lprogressHorizontal: View
    lateinit var btnSave: Button
    lateinit var btnUpdate: Button

    lateinit var spP11: Spinner
    lateinit var spP12: Spinner
    lateinit var spP13: Spinner
    lateinit var spP14: Spinner
    lateinit var spP15: Spinner
    lateinit var spP16: Spinner
    lateinit var spP17: Spinner
    lateinit var spP18: Spinner
    lateinit var spP21: Spinner
    lateinit var spP22: Spinner
    lateinit var spP23: Spinner
    lateinit var spP24: Spinner
    var flag = -2
    val display = Display()

    lateinit var spAd11: ArrayAdapter<String>
    lateinit var spAd12: ArrayAdapter<String>
    lateinit var spAd13: ArrayAdapter<String>
    lateinit var spAd14: ArrayAdapter<String>
    lateinit var spAd15: ArrayAdapter<String>
    lateinit var spAd16: ArrayAdapter<String>
    lateinit var spAd17: ArrayAdapter<String>
    lateinit var spAd18: ArrayAdapter<String>
    lateinit var spAd21: ArrayAdapter<String>
    lateinit var spAd22: ArrayAdapter<String>
    lateinit var spAd23: ArrayAdapter<String>
    lateinit var spAd24: ArrayAdapter<String>

    lateinit var btndate1: Button
    lateinit var btndate2: Button
    lateinit var btndate3: Button
    lateinit var btndate4: Button
    lateinit var btndate5: Button
    lateinit var btndate6: Button
    lateinit var btndate7: Button
    lateinit var btndate8: Button

    lateinit var btndateq1: Button
    lateinit var btndateq2: Button

    lateinit var btndates1: Button

    val const = Const()

    //for updation
    lateinit var matchidArray: ArrayList<String>
    lateinit var dateArray: ArrayList<String>
    lateinit var dep1Array: ArrayList<String>
    lateinit var dep2Array: ArrayList<String>
    lateinit var sem1Array: ArrayList<String>
    lateinit var sem2Array: ArrayList<String>
    lateinit var tArray: ArrayList<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_group_aschedule, container, false)
        initializeObjects()

        getTeams()


        if(prefs.getBoolean(const.IS_SCHEDULE_ADDED_SHARED_PREF,false)) {
            Toast.makeText(rootView.context,"jjjjjjjjj",Toast.LENGTH_LONG).show()
            btnSave.visibility = View.GONE
            btnUpdate.visibility = View.VISIBLE
        }else{
            btnSave.visibility = View.VISIBLE
            btnUpdate.visibility = View.GONE
        }

        for (i in spinnerArray){
            i.onItemSelectedListener = this
        }

        btnSave.setOnClickListener {
            addSchedule(const.ADD_SCHEDULE_A_URL)
        }
        btnUpdate.setOnClickListener {
            updateShedule()
        }

        for(i in buttonArray){
            i.setOnClickListener(this)
        }
        return rootView
    }

    //to do
    private fun displaySchedule() {
        lprogressHorizontal.visibility = View.VISIBLE

        //call the webservice
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_MY_SCHEDULE_GROUP_URL,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        lprogressHorizontal.visibility = View.GONE
                        var jsonArray: JSONArray = jsonObject.getJSONArray("schedule")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val matchid = jsonObject1.getString("match_id")
                            val date = jsonObject1.getString("m_date")
                            val dep1 = jsonObject1.getString("dep1")
                            val dep2 = jsonObject1.getString("dep2")
                            val sem1 = jsonObject1.getString("sem1")
                            val sem2 = jsonObject1.getString("sem2")

                            matchidArray.add(matchid)
                            dateArray.add(date)
                            dep1Array.add(dep1)
                            dep2Array.add(dep2)
                            sem1Array.add(sem1)
                            sem2Array.add(sem2)

                            tArray.add("S$sem1-$dep1")
                            tArray.add("S$sem2-$dep2")
                        }
                        spP11.setSelection(spAd11.getPosition(tArray[0]))
                        spP12.setSelection(spAd11.getPosition(tArray[1]))
                        spP13.setSelection(spAd11.getPosition(tArray[2]))
                        spP14.setSelection(spAd11.getPosition(tArray[3]))
                        spP15.setSelection(spAd11.getPosition(tArray[4]))
                        spP16.setSelection(spAd11.getPosition(tArray[5]))
                        spP17.setSelection(spAd11.getPosition(tArray[6]))
                        spP18.setSelection(spAd11.getPosition(tArray[7]))

                        spP21.setSelection(spAd11.getPosition(tArray[8]))
                        spP22.setSelection(spAd11.getPosition(tArray[9]))
                        spP23.setSelection(spAd11.getPosition(tArray[10]))
                        spP24.setSelection(spAd11.getPosition(tArray[11]))

                        btndate1.text = dateArray[0]
                        btndate3.text = dateArray[1]
                        btndate5.text = dateArray[2]
                        btndate7.text = dateArray[3]

                        btndate2.text = dateArray[4]
                        btndate4.text = dateArray[5]
                        btndate6.text = dateArray[6]
                        btndate8.text = dateArray[7]

                        btndateq1.text = dateArray[8]
                        btndateq2.text = dateArray[9]

                        btndates1.text = dateArray[10]
                    }
                } catch (e: Exception) {
                    lprogressHorizontal.visibility = View.GONE
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                lprogressHorizontal.visibility = View.GONE
                Toast.makeText(rootView.context,it.toString(),
                    Toast.LENGTH_SHORT).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val sharedPreferences = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                val params = HashMap<String,String>()
                params["email"] = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()
                params["group"] = "a"
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    private fun updateShedule() {

    }


    override fun onClick(parent: View?) {
        if (parent!!.id==R.id.btn_sa_save){

        }else{
            var button = rootView.findViewById<Button>(parent.id)
            showDatePickerDialog(button)
        }
    }

    private fun getTeams() {
        lprogressHorizontal.visibility = View.VISIBLE

        //call the webservice
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_TEAMS_URL,
            Response.Listener<String> { response ->
                Log.e("rq",response)
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        lprogressHorizontal.visibility = View.GONE
                        var jsonArray: JSONArray = jsonObject.getJSONArray("teams")
                        depArray.add("Team")
                        semArray.add("")
                        teamidArray.add("0")
                        teamArray.add("Team")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val department = jsonObject1.getString("dep_name")
                            val sem = jsonObject1.getString("semester")
                            val tid = jsonObject1.getString("team_id")
                            depArray.add(department)
                            semArray.add(sem)
                            teamidArray.add(tid)
                            teamArray.add("S$sem-$department")
                        }
                        setAdapter()
                        if(prefs.getBoolean(const.IS_SCHEDULE_ADDED_SHARED_PREF,false)) {
                            displaySchedule()
                        }
                    }
                } catch (e: Exception) {
                    lprogressHorizontal.visibility = View.GONE
                    Toast.makeText(activity, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                lprogressHorizontal.visibility = View.GONE
                Toast.makeText(rootView.context,it.toString(),
                    Toast.LENGTH_SHORT).show() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val sharedPreferences = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                val params = HashMap<String,String>()
                params["email"] = sharedPreferences.getString(const.EMAIL_SHARED_PREF,"").toString()
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    private fun setAdapter() {
        initializeAdapter()

        adapterArray.add(spAd11)
        adapterArray.add(spAd12)
        adapterArray.add(spAd13)
        adapterArray.add(spAd14)
        adapterArray.add(spAd15)
        adapterArray.add(spAd16)
        adapterArray.add(spAd17)
        adapterArray.add(spAd18)
        adapterArray.add(spAd21)
        adapterArray.add(spAd22)
        adapterArray.add(spAd23)
        adapterArray.add(spAd24)

        for(i in 0..11){
            spinnerArray[i].adapter = adapterArray[i]
            spinnerArray[i].setSelection(adapterArray[i].getPosition("Team"))
        }
        for(i in 0..11){
            spinnerArray[i].tag = 1
        }
    }

    private fun initializeAdapter() {
        spAd11 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd12 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd13 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd14 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd15 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd16 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd17 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd18 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd21 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd22 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd23 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
        spAd24 = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            teamArray
        )
    }

    private fun initializeObjects() {
        teamidArray = ArrayList()
        depArray = ArrayList()
        semArray = ArrayList()
        spinnerArray = ArrayList()
        teamArray = ArrayList()
        prefs = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
        lprogressHorizontal = rootView.findViewById(R.id.pb_sa_loading)
        spP11 = rootView.findViewById(R.id.sp_sa_11)
        spP12 = rootView.findViewById(R.id.sp_sa_12)
        spP13 = rootView.findViewById(R.id.sp_sa_13)
        spP14 = rootView.findViewById(R.id.sp_sa_14)
        spP15 = rootView.findViewById(R.id.sp_sa_15)
        spP16 = rootView.findViewById(R.id.sp_sa_16)
        spP17 = rootView.findViewById(R.id.sp_sa_17)
        spP18 = rootView.findViewById(R.id.sp_sa_18)
        spP21 = rootView.findViewById(R.id.sp_sa_21)
        spP22 = rootView.findViewById(R.id.sp_sa_22)
        spP23 = rootView.findViewById(R.id.sp_sa_23)
        spP24 = rootView.findViewById(R.id.sp_sa_24)

        btndate1 = rootView.findViewById(R.id.date_sa_p11)
        btndate2 = rootView.findViewById(R.id.date_sa_p21)
        btndate3 = rootView.findViewById(R.id.date_sa_p12)
        btndate4 = rootView.findViewById(R.id.date_sa_p22)
        btndate5 = rootView.findViewById(R.id.date_sa_p13)
        btndate6 = rootView.findViewById(R.id.date_sa_p23)
        btndate7 = rootView.findViewById(R.id.date_sa_p14)
        btndate8 = rootView.findViewById(R.id.date_sa_p24)

        btndateq1 = rootView.findViewById(R.id.date_sa_q1)
        btndateq2 = rootView.findViewById(R.id.date_sa_q2)

        btndates1 = rootView.findViewById(R.id.date_sa_s1)

        btnSave = rootView.findViewById(R.id.btn_sa_save)
        btnUpdate = rootView.findViewById(R.id.btn_sa_update)

        spinnerArray.add(spP11)
        spinnerArray.add(spP12)
        spinnerArray.add(spP13)
        spinnerArray.add(spP14)
        spinnerArray.add(spP15)
        spinnerArray.add(spP16)
        spinnerArray.add(spP17)
        spinnerArray.add(spP18)
        spinnerArray.add(spP21)
        spinnerArray.add(spP22)
        spinnerArray.add(spP23)
        spinnerArray.add(spP24)

        adapterArray = ArrayList()
        buttonArray = ArrayList()

        buttonArray.add(btndate1)
        buttonArray.add(btndate2)
        buttonArray.add(btndate3)
        buttonArray.add(btndate4)
        buttonArray.add(btndate5)
        buttonArray.add(btndate6)
        buttonArray.add(btndate7)
        buttonArray.add(btndate8)

        buttonArray.add(btndateq1)
        buttonArray.add(btndateq2)

        buttonArray.add(btndates1)

        //for updation
        matchidArray = ArrayList()
        dateArray = ArrayList()
        sem1Array = ArrayList()
        sem2Array = ArrayList()
        dep1Array = ArrayList()
        dep2Array = ArrayList()
        tArray = ArrayList()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) { }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        /*val spId:  String = parent?.id.toString()
        val spinner = rootView.findViewById<Spinner>(parent!!.id)
        val selected = parent?.selectedItem.toString()
        flag++
        Log.e("tag ", parent.tag.toString())

        if (selected!="Team" && parent.tag !=123*//*(flag%11==0)&&flag>=11*//*){
            parent.tag = 1
            updateSpinner(spinner,selected)
        }else{
            parent.tag = 1
        }*/
        /*if (spId.toInt() == R.id.sp_sa_11){
           // department = spDepartment.getItemAtPosition(position).toString()

        }*/
    }

    private fun addSchedule(url: String){
        val stringRequest = object : StringRequest(
            Method.POST, url,
            Response.Listener<String> { response ->
                try {
                    if(response.trim().equals(const.SUCCESS)){
                        display.showSuccessToast(rootView,"Group a schedule created..",Gravity.BOTTOM,0,200)
                        findNavController().navigate(R.id.fragmentGroupBSchedule)
                    }else{
                        display.showErrorToast(rootView,response,Gravity.BOTTOM,0,200)
                    }
                }catch (e: Exception){
                    Toast.makeText(rootView.context,e.toString(),Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener {
                Toast.makeText(rootView.context, it.toString(), Toast.LENGTH_LONG).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["email"] = prefs.getString(const.EMAIL_SHARED_PREF, "").toString()

                params["t11"] = getTeamId(spP11.selectedItem.toString())
                params["t12"] = getTeamId(spP12.selectedItem.toString())
                params["t13"] = getTeamId(spP21.selectedItem.toString())

                params["t21"] = getTeamId(spP13.selectedItem.toString())
                params["t22"] = getTeamId(spP14.selectedItem.toString())
                params["t23"] = getTeamId(spP22.selectedItem.toString())

                params["t31"] = getTeamId(spP15.selectedItem.toString())
                params["t32"] = getTeamId(spP16.selectedItem.toString())
                params["t33"] = getTeamId(spP23.selectedItem.toString())

                params["t41"] = getTeamId(spP17.selectedItem.toString())
                params["t42"] = getTeamId(spP17.selectedItem.toString())
                params["t43"] = getTeamId(spP24.selectedItem.toString())

                params["date1"] = convertDate(btndate1.text.toString())
                params["date2"] = convertDate(btndate2.text.toString())
                params["date3"] = convertDate(btndate3.text.toString())
                params["date4"] = convertDate(btndate4.text.toString())
                params["date5"] = convertDate(btndate5.text.toString())
                params["date6"] = convertDate(btndate6.text.toString())
                params["date7"] = convertDate(btndate7.text.toString())
                params["date8"] = convertDate(btndate8.text.toString())

                params["dateq1"] = convertDate(btndateq1.text.toString())
                params["dateq2"] = convertDate(btndateq2.text.toString())

                params["dates1"] = convertDate(btndates1.text.toString())

                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    private fun updateSpinner(skip: Spinner,str: String){
      //  Log.e("sett ", str + " : " + skip.toString())
        flag--
        for(i in 0..11){
            if(spinnerArray[i].id!=skip.id){
                val selection = spinnerArray[i].selectedItem.toString()
               // Log.e("tag ", selection)
                adapterArray[i].remove(str)
                adapterArray[i].notifyDataSetChanged()
               /* spinnerArray[i].adapter = adapterArray[i]*/
                spinnerArray[i].setSelection(adapterArray[i].getPosition(selection))
                spinnerArray[i].tag = 123
                if(selection=="Team"){
                    onItemSelected(spinnerArray[i],null,i,spinnerArray[i].id.toLong())
                }
            }

        }

    }

    private fun getTeamId(str: String): String{
        var index = teamArray.indexOf(str)
        return teamidArray[index].toString()
    }

    fun showDatePickerDialog(btn: Button) {
        val fm = (activity as AppCompatActivity).supportFragmentManager
        val newFragment = Display.DatePickerFragment(rootView,btn)
        newFragment.show(fm, "datePicker")
    }

    private fun convertDate(date: String): String {
        var newDate: String
        val initDate = SimpleDateFormat("dd/MM/yyyy").parse(date)
        newDate = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(initDate)
        return newDate
    }

}
