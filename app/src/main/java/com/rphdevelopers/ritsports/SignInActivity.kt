package com.rphdevelopers.ritsports

import android.app.Activity
import android.os.Bundle
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import android.accounts.AccountManager.KEY_PASSWORD
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.json.JSONObject

class SignInActivity : Activity() {

    lateinit var etUEmail: EditText
    lateinit var etPassword: EditText
    lateinit var btnSignIn: Button
    lateinit var btnClose: ImageButton
    lateinit var lprogressbar: View
    lateinit var const: Const

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        const = Const()
        etUEmail = findViewById(R.id.etUemail)
        etPassword = findViewById(R.id.etPassword)
        btnSignIn = findViewById(R.id.btnSignIn)
        btnClose = findViewById(R.id.btnCloseSignIn)

        lprogressbar = findViewById(R.id.llProgressBar)


        btnSignIn.setOnClickListener { login() }
        btnClose.setOnClickListener { finish() }
    }

    private fun login() {
        Log.e("jio","login")
        val email = etUEmail.text.toString()
        val password = etPassword.text.toString()

        //to show loading ....
        llProgressBar.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Method.POST, const.LOGIN_URL,
            Response.Listener<String> { response ->
                try {
                    Log.e("jio","first: $response")
                    val LOGIN_SUCCESS = "success"
                    val jsonObject = JSONObject(response)
                    if (jsonObject.getString("log").trim { it <= ' ' }.equals(LOGIN_SUCCESS, ignoreCase = true)) {
                         if(jsonObject.getString("role").toString()==const.ROLE_TEAM_MANAGER){
                             checkPlayerAdded(email,jsonObject.getString("role"),jsonObject.getString("status"))
                         }else if(jsonObject.getString("role").toString()==const.ROLE_TOUR_COORDINATOR){
                             setSharedPreferences(email,jsonObject.getString("role"),jsonObject.getString("status"),false)
                             checkScheduleAdded()
                         }
                         else{
                            setSharedPreferences(email,jsonObject.getString("role"),jsonObject.getString("status"),false)
                            llProgressBar.visibility = View.GONE
                            finish()
                        }

                    } else {
                        llProgressBar.visibility = View.GONE
                        Toast.makeText(
                            applicationContext,
                            "Invalid username or password",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext,"Something went wrong!",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { llProgressBar.visibility = View.GONE }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val prams = HashMap<String,String>()
                val KEY_EMAIL = "email"
                prams[KEY_EMAIL] = email
                prams[KEY_PASSWORD] = password
                return prams
            }
        }
        val requestQueue = Volley.newRequestQueue(this)
        requestQueue.add(stringRequest)
        Log.e("jio","first request")

    }

    private fun checkScheduleAdded() {
        //###TO CHECK WHETHER ALREADY ADDED A PLAYER OR NOT
        val stringRequest2 = object : StringRequest(
            Method.POST, const.IS_SCHEDULE_ADDED_URL,
            Response.Listener<String> { response ->
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        setSharedPrefSchedule(true)

                    }else {
                        setSharedPrefSchedule(false)
                    }
                    llProgressBar.visibility = View.GONE
                    finish()
                } catch (e: Exception) {
                    Toast.makeText(applicationContext,e.toString(),Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { llProgressBar.visibility = View.GONE }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val prams = HashMap<String,String>()
                prams["email"] = etUEmail.text.toString()
                return prams
            }
        }
        val requestQueue2 = Volley.newRequestQueue(this)
        requestQueue2.add(stringRequest2)
        Log.e("jio","second request")

        //###END CHECK
    }

    private fun setSharedPreferences(email: String,role: String,status: String,added: Boolean) {
        val sharedPreferences = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(const.LOGGEDIN_SHARED_PREF, true)
        editor.putString(const.EMAIL_SHARED_PREF, email)
        editor.putString(const.LOGGEDIN_ROLE_SHARED_PREF,role)
        editor.putString(const.STATUS_SHARED_PREF,status)
        editor.putBoolean(const.IS_PLAYER_ADDED_SHARED_PREF, added)
        editor.commit()
        Log.e("jio:s",sharedPreferences.getBoolean(const.LOGGEDIN_SHARED_PREF,false).toString())
        Log.e("jio",sharedPreferences.getString(const.LOGGEDIN_ROLE_SHARED_PREF,"NOOO").toString())
    }

    private fun setSharedPrefSchedule(flag: Boolean) {
        val sharedPreferences = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(const.IS_SCHEDULE_ADDED_SHARED_PREF, true)
        editor.commit()
    }

    private fun checkPlayerAdded(email: String,role: String,status: String) {
        //###TO CHECK WHETHER ALREADY ADDED A PLAYER OR NOT
        val stringRequest2 = object : StringRequest(
            Method.POST, const.IS_PLAYER_ADDED_URL,
            Response.Listener<String> { response ->
                try {
                    if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                        setSharedPreferences(email,role,status,true)

                    }else {
                        setSharedPreferences(email,role,status,false)
                    }
                    llProgressBar.visibility = View.GONE
                    finish()
                } catch (e: Exception) {
                    Toast.makeText(applicationContext,e.toString(),Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { llProgressBar.visibility = View.GONE }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val prams = HashMap<String,String>()
                prams["email"] = email
                return prams
            }
        }
        val requestQueue2 = Volley.newRequestQueue(this)
        requestQueue2.add(stringRequest2)
        Log.e("jio","second request")

        //###END CHECK
    }
}