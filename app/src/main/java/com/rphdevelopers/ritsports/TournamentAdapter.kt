package com.rphdevelopers.ritsports

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.coordinator_list_text_view.view.*
import kotlinx.android.synthetic.main.tournament_list_home.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TournamentAdapter(private val ptidArray: ArrayList<String>,private val spNameArray: ArrayList<String>, private val tNameArray: ArrayList<String>,private val pcountArray: ArrayList<String>,
                        private val sdateArray: ArrayList<String>,private val edateArray: ArrayList<String>,private val coordinatorArray: ArrayList<String>,
                        private val context: Context, private val lprogress: View, onItemListener: OnItemListener):
        RecyclerView.Adapter<TournamentAdapter.MyViewHolder>(){

    val const = Const()
    var onItemListener: OnItemListener

    init {
        this.onItemListener = onItemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.tournament_list_home,parent,false)

        return MyViewHolder(view,onItemListener)
    }

    override fun getItemCount() = ptidArray.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val stid = ptidArray[position]
        val stname = tNameArray[position]
        val scount = pcountArray[position]
        val ssdate = sdateArray[position]
        val sedate = edateArray[position]
        val scoord = coordinatorArray[position]
        val sport = spNameArray[position]

        holder.tournamentName.text = stname
        holder.tid.text = stid
        holder.sdate.text = getDate(ssdate)
        holder.edate.text = getDate(sedate)
        holder.coordinator.text = "Conducted by: " + scoord.capitalize() + " department"
        if(sport==const.FOOTBALL){
            holder.count.text = scount + "v" + scount + " Football tournament"
        }else if(sport=="Cricket"){
            holder.count.text = "Cricket Tournament"
        }
    }

    private fun getDate(date: String): String{
        var newDate: String
        val initDate = SimpleDateFormat("yyyy-MM-dd").parse(date)
        newDate = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(initDate)
        return newDate
    }

    class MyViewHolder(view: View, onItemListener: OnItemListener): RecyclerView.ViewHolder(view),View.OnClickListener{
        var onItemListener:OnItemListener

        init {
            view.setOnClickListener(this)
            this.onItemListener = onItemListener
        }
        val tournamentName = view.tv_t_name
        val count = view.tv_t_count
        val sdate = view.tv_t_sdate
        val edate = view.tv_t_edate
        val coordinator = view.tv_t_coordinator
        val tid = view.tv_t_id

        override fun onClick(view: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    interface OnItemListener{
        fun onItemClick(position: Int)
    }
}
