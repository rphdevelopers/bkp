package com.rphdevelopers.ritsports

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_schedule.*

class ScheduleActivity : AppCompatActivity() {

    lateinit var bottomNav: BottomNavigationView
    lateinit var toolbar: Toolbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)

        bottomNav = findViewById(R.id.bottom_nav)
        val navController = findNavController(R.id.bottom_nav_host_fragment)
        bottomNav.setupWithNavController(navController)

        setSupportActionBar(toolbar_as)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
        return super.onSupportNavigateUp()
    }
}
