package com.rphdevelopers.ritsports

import android.graphics.Color
import kotlinx.android.synthetic.main.players_list.view.*

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.team_players_list.view.*


class TeamPlayersAdapter(private val name: ArrayList<String>, val lprogress: View, onItemListener: OnItemListener, val rootView: View):
    RecyclerView.Adapter<TeamPlayersAdapter.MyViewHolder>(){


    val const = Const()
    var onItemListener: OnItemListener

    init {
        this.onItemListener = onItemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.team_players_list,parent,false)

        return MyViewHolder(view,onItemListener)
    }

    override fun getItemCount() = name.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvName.text = name[position]
    }

    class MyViewHolder(view: View, onItemListener: OnItemListener): RecyclerView.ViewHolder(view),View.OnClickListener{
        var onItemListener:OnItemListener

        init {
            view.setOnClickListener(this)
            this.onItemListener = onItemListener
        }
        val tvName = view.tv_tpl_player

        override fun onClick(view: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    interface OnItemListener{
        fun onItemClick(position: Int)
    }
}
