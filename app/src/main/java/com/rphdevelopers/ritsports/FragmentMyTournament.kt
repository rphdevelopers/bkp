package com.rphdevelopers.ritsports


import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * A simple [Fragment] subclass.
 */
class FragmentMyTournament : Fragment(), AdapterView.OnItemSelectedListener {
    lateinit var rootView: View
    lateinit var btnSdate: Button
    lateinit var btnEdate: Button
    lateinit var btnUpdate: Button
    lateinit var etTname: EditText
    lateinit var spStype: Spinner
    lateinit var spFtype: Spinner
    lateinit var spPcount: Spinner
    lateinit var llPcount: LinearLayout
    lateinit var tilName: TextInputLayout
    lateinit var tilSdate: TextInputLayout
    lateinit var tilEdate: TextInputLayout
    lateinit var tilFee: TextInputLayout
    lateinit var tilMperday: TextInputLayout
    lateinit var etFee: EditText
    lateinit var etMperday: EditText
    lateinit var tvId: TextView
    lateinit var llNoTour: LinearLayout
    lateinit var progressBar: View
    lateinit var sharedPrefs: SharedPreferences
    var nplayerArray = ArrayList<String>()

    val loader = Loader()
    val const = Const()
    val volleyRequests = VolleyRequests()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_my_tournament, container, false)

        initializeObjects()

        //FOR THREES/SEVENS/ELEVENS
        nplayerArray.add("3")
        nplayerArray.add("7")
        nplayerArray.add("9")

        spStype.onItemSelectedListener = this

        //SET SPINNER BY CHAINING ONE BY ONE AFTER ONE
        setPlayerCountSpinner()
        setSportType()

        btnSdate.setOnClickListener {
            showDatePickerDialog(btnSdate)
        }
        btnEdate.setOnClickListener {
            showDatePickerDialog(btnEdate)
        }
        btnUpdate.setOnClickListener {
            val name = etTname.text.toString()
            val stype = spStype.selectedItem.toString()
            var numPlayers = "0"
            if(stype==const.CRICKET)
                numPlayers = "11"
            else
                numPlayers = spPcount.selectedItem.toString()
            val fixType = spFtype.selectedItem.toString()
            val sdate = btnSdate.text.toString()
            val edate = btnEdate.text.toString()
            if(name.isEmpty())
                tilName.error = "Name required"
            else if(sdate==resources.getString(R.string.start_date))
                tilSdate.error = "Date needed"
            else if(edate==resources.getString(R.string.end_date))
                tilEdate.error = "Date needed"
            else if(etMperday.text.toString().isEmpty())
                tilMperday.error = "Match per day required"
            else if(etFee.text.toString().isEmpty())
                tilFee.error = "Fee is required"
            else{
                val params = HashMap<String,String>()
                params["email"] = sharedPrefs.getString(const.EMAIL_SHARED_PREF, "").toString()
                params["sp_name"] = spStype.selectedItem.toString()
                params["fix_name"] = spFtype.selectedItem.toString()
                params["tou_name"] = etTname.text.toString()
                params["mper_day"] = etMperday.text.toString()
                params["s_date"] = convertDate(btnSdate.text.toString())
                params["e_date"] = convertDate(btnEdate.text.toString())
                params["fee"] = etFee.text.toString()
                if(stype=="Cricket")
                    params["p_count"] = "11"
                else
                    params["p_count"] = spPcount.selectedItem.toString()


                volleyRequests.insert(const.UPDATE_TOURNAMENT_URL,rootView,params,"Successfully Updated")
            }

        }
        return rootView
    }

    //TO INITIALIZE ALL OBJECTS INCLUDEING - VIEW, VARIABLE, ETC..
    private fun initializeObjects() {
        btnSdate = rootView.findViewById(R.id.btn_mt_sdate)
        btnEdate = rootView.findViewById(R.id.btn_mt_edate)
        btnUpdate = rootView.findViewById(R.id.btn_mt_update_tour)
        etTname = rootView.findViewById(R.id.et_mt_name)
        spFtype = rootView.findViewById(R.id.sp_mt_ftype)
        spPcount = rootView.findViewById(R.id.sp_mt_pcount)
        spStype = rootView.findViewById(R.id.sp_mt_type)
        tvId = rootView.findViewById(R.id.tv_mt_id)
        llPcount = rootView.findViewById(R.id.ll_mt_nplayers)
        tilName = rootView.findViewById(R.id.til_mt_name)
        tilSdate = rootView.findViewById(R.id.til_mt_sdate)
        tilEdate = rootView.findViewById(R.id.til_mt_edate)
        etMperday = rootView.findViewById(R.id.et_mt_mperday)
        llNoTour = rootView.findViewById(R.id.ll_no_tour)
        etFee = rootView.findViewById(R.id.et_mt_fee)
        tilMperday = rootView.findViewById(R.id.til_mt_mperday)
        progressBar = rootView.findViewById(R.id.pb_mt_loading)
        tilFee = rootView.findViewById(R.id.til_mt_fee)

        sharedPrefs = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    private fun setSportType(){
        progressBar.visibility = View.VISIBLE
        var arrayList = ArrayList<String>()
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_SPORTS_URL,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("sports")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("spname")
                            arrayList.add(name)
                        }
                    }
                    var adapter = ArrayAdapter<String>(
                        rootView.context,
                        android.R.layout.simple_spinner_dropdown_item,
                        arrayList
                    )
                    spStype.setAdapter(adapter)
                    setFixtureType()
                } catch (e: Exception) {
                    Toast.makeText(rootView.context, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { Toast.makeText(rootView.context,"Something went wrong...",
                Toast.LENGTH_SHORT).show() }) {
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

    fun setFixtureType(){
        var arrayList = ArrayList<String>()
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_FIXTURE_URL,
            Response.Listener<String> { response ->
                try {
                    var jsonObject = JSONObject(response)
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("fixture")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject1 = jsonArray.getJSONObject(i)
                            val name = jsonObject1.getString("fix_type")
                            arrayList.add(name)
                        }
                    }
                    var adapter = ArrayAdapter<String>(
                        rootView.context,
                        android.R.layout.simple_spinner_dropdown_item,
                        arrayList
                    )
                    spFtype.adapter = adapter
                    loadData()
                } catch (e: Exception) {
                    Toast.makeText(rootView.context, "$e#EXCEPTION#", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { Toast.makeText(rootView.context,"Something went wrong...",
                Toast.LENGTH_SHORT).show() }) {
        }
        val requestQueue = Volley.newRequestQueue(rootView.context)
        requestQueue.add(stringRequest)
    }

    //DISPLAY THE PREVIOUS DATA
    private fun loadData() {
        val stringRequest = object : StringRequest(
            Method.POST, const.GET_MY_TOURNAMENT_URL,
            Response.Listener<String> { response ->
                progressBar.visibility = View.GONE
                var jsonObject = JSONObject(response)
                try {
                    if(jsonObject.getString("log")=="success"){
                        var jsonArray: JSONArray = jsonObject.getJSONArray("tournament")
                        if(jsonArray.length()==0){
                            llNoTour.visibility = View.VISIBLE
                        }else {
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject1 = jsonArray.getJSONObject(i)
                                llNoTour.visibility = View.GONE
                                val tid = jsonObject1.getString("tou_id")
                                val spname = jsonObject1.getString("spname")
                                if(spname==const.CRICKET){
                                    llPcount.visibility = View.GONE
                                }else{
                                    llPcount.visibility = View.VISIBLE
                                    val pcount = jsonObject1.getString("player_count")
                                    spPcount.setSelection(getSpinnerPosition(spPcount, pcount))

                                }
                                val tname = jsonObject1.getString("tou_name")
                                val sdate = jsonObject1.getString("start_date")
                                val edate = jsonObject1.getString("end_date")
                                val fixtype = jsonObject1.getString("fix_type")
                                val mperday = jsonObject1.getString("match_per_day")
                                val fee = jsonObject1.getString("fee")

                                tvId.text = tid
                                spStype.setSelection(getSpinnerPosition(spStype, spname))
                                etTname.setText(tname)
                                btnSdate.text = convertDate2(sdate)
                                btnEdate.text = convertDate2(edate)
                                spFtype.setSelection(getSpinnerPosition(spFtype, fixtype))
                                etMperday.setText(mperday)
                                etFee.setText(fee)
                            }
                        }
                    }else{
                        Toast.makeText(rootView.context,response+" last",Toast.LENGTH_LONG).show()
                    }
                }catch (e: Exception){}
            },
            Response.ErrorListener {
                Toast.makeText(rootView.context,"Something went wrong...",
                    Toast.LENGTH_SHORT).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["email"] = sharedPrefs.getString(const.EMAIL_SHARED_PREF,"").toString()
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)
    }

    //TO GET THE POSITION OF THE SPINNER BY VALUE
    private fun getSpinnerPosition(spinner: Spinner,str: String): Int {
        for (i in 0..spinner.count){
            if(spinner.getItemAtPosition(i).toString()==str)
                return i
        }
        return 0
    }

    //TO CONVERT THE DATE TO DIFF FORMAT
    private fun convertDate(date: String): String {
        var newDate: String
        val initDate = SimpleDateFormat("dd/MM/yyyy").parse(date)
        newDate = SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH).format(initDate)
        return newDate
    }
    //CONVERT TO DDMMYYY
    private fun convertDate2(date: String): String {
        var newDate: String
        val initDate = SimpleDateFormat("yyyy-MM-dd").parse(date)
        newDate = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(initDate)
        return newDate
    }

    //TO SHOW DATE PICKER
    private fun showDatePickerDialog(btn: Button) {
        val fm = (activity as AppCompatActivity).supportFragmentManager
        val newFragment = Display.DatePickerFragment(rootView,btn)
        newFragment.show(fm, "datePicker")
    }

    //SPINNER ON ITEM SELECTED LISTENER
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val spId: String = parent?.id.toString()
        if (spId.toInt() == R.id.sp_at_type) {
            val sportType = spStype.getItemAtPosition(position).toString()
            if(sportType=="Cricket") {
                llPcount.visibility = View.GONE
            }
            else {
                llPcount.visibility = View.VISIBLE
                setPlayerCountSpinner()
            }
        }
    }

    private fun setPlayerCountSpinner() {
        var adapter = ArrayAdapter<String>(
            rootView.context,
            android.R.layout.simple_spinner_dropdown_item,
            nplayerArray
        )
        spPcount.adapter = adapter
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}