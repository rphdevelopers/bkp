package com.rphdevelopers.ritsports

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.teams_list.view.*

class TeamsAdapter(val semesterArray: ArrayList<String>, val departmentArray: ArrayList<String>,
                   val teamArray: ArrayList<String>, val nameArray: ArrayList<String>, val mobileArray: ArrayList<String>,
                   val context: Context, val lprogress: View,
                   onItemListener: OnItemListener):
    RecyclerView.Adapter<TeamsAdapter.MyViewHolder>(){

    val const = Const()
    var onItemListener: OnItemListener

    init {
        this.onItemListener = onItemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.teams_list,parent,false)

        return MyViewHolder(view,onItemListener)
    }

    override fun getItemCount() = nameArray.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val sem = semesterArray[position]
        val dep = departmentArray[position]

        holder.department.text = "S$sem - $dep"
        holder.mobile.text = "Mobile : " + mobileArray[position]
        holder.name.text = "Team manager : " + nameArray[position]
        holder.teamName.text = "Team Name : " + teamArray[position]
    }

    class MyViewHolder(view: View, onItemListener: OnItemListener): RecyclerView.ViewHolder(view),
        View.OnClickListener{
        var onItemListener:OnItemListener

        init {
            view.setOnClickListener(this)
            this.onItemListener = onItemListener
        }
        val department = view.tv_tl_department
        val teamName = view.tv_tl_team_name
        val name = view.tv_tl_name
        val mobile = view.tv_tl_mobile

        override fun onClick(view: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    interface OnItemListener{
        fun onItemClick(position: Int)
    }
}