package com.rphdevelopers.ritsports

import android.graphics.Color
import kotlinx.android.synthetic.main.players_list.view.*

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class PlayerAdapter(private val name: ArrayList<String>, val lprogress: View, onItemListener: OnItemListener,val rootView: View):
    RecyclerView.Adapter<PlayerAdapter.MyViewHolder>(){


    val const = Const()
    var onItemListener: OnItemListener

    init {
        this.onItemListener = onItemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.players_list,parent,false)

        return MyViewHolder(view,onItemListener)
    }

    override fun getItemCount() = name.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvName.text = name[position]
        if (position%2==0)
            holder.tvName.setBackgroundColor(Color.parseColor("#E719691B"))
        else
            holder.tvName.setBackgroundColor(Color.parseColor("#E70C420E"))
    }

    class MyViewHolder(view: View, onItemListener: OnItemListener): RecyclerView.ViewHolder(view),View.OnClickListener{
        var onItemListener:OnItemListener

        init {
            view.setOnClickListener(this)
            this.onItemListener = onItemListener
        }
        val tvName = view.tv_pl_player

        override fun onClick(view: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    interface OnItemListener{
        fun onItemClick(position: Int)
    }
}
