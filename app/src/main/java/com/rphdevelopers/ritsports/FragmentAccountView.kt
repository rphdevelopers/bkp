package com.rphdevelopers.ritsports


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_update_coordinator.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 */
class FragmentAccountView : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var fabEdit: FloatingActionButton
    lateinit var btnChangePassword: Button
    lateinit var etUname: EditText
    lateinit var etUemail: EditText
    lateinit var etUmob: EditText
    lateinit var llDepartment: LinearLayout
    lateinit var spinner: Spinner
    lateinit var btnUpdate: Button

    lateinit var rootView: View
    lateinit var const: Const
    lateinit var prefs: SharedPreferences
    lateinit var uid: String
    lateinit var from: String
    lateinit var role: String
    val loader = Loader()
    lateinit var departmentName: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_account_view, container, false)
        const = Const()

        //INITIALIE VIEWS
        fabEdit = rootView.findViewById(R.id.fab_av_edit)
        btnChangePassword = rootView.findViewById(R.id.btn_av_change)
        etUname = rootView.findViewById(R.id.et_av_username)
        etUemail = rootView.findViewById(R.id.et_av_uemail)
        btnUpdate = rootView.findViewById(R.id.btn_av_update)
        etUmob = rootView.findViewById(R.id.et_av_umobile)
        llDepartment = rootView.findViewById(R.id.ll_av_department)
        spinner = rootView.findViewById(R.id.sp_av_department)

        prefs = rootView.context.getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
        role = prefs.getString(const.LOGGEDIN_ROLE_SHARED_PREF, null).toString()
        from = arguments?.getString("from").toString()

        //TO SET THE USER ID TO  IDENTIFY THE ACTION - BY ADMIN/OTHER/ADMIN TO OTHER
        val id = arguments?.getInt("uid")
        if(from==const.FROM_PARENT){
            llDepartment.visibility = View.GONE
            uid = prefs.getString(const.UID_SHARED_PREF, null).toString()
        }else{
            llDepartment.visibility = View.VISIBLE
            uid = arguments?.getInt("uid").toString()
        }

        //SET USER DETAILS
        setData()

        spinner.onItemSelectedListener = this

        //OCNCLICK EVENTS
        btnChangePassword.setOnClickListener{
            var bundle = bundleOf("uid" to uid.toInt(), "from" to from)
            findNavController().navigate(R.id.fragment_change_password, bundle)
        }

        //fab click
        fabEdit.setOnClickListener{
            btnUpdate.visibility = if(btnUpdate.visibility==View.VISIBLE) View.GONE else View.VISIBLE
            if(btnUpdate.visibility==View.VISIBLE){
                etUname.isEnabled = true
                etUemail.isEnabled = true
                etUmob.isEnabled = true
            }else {
                etUname.isEnabled = false
                etUemail.isEnabled = false
                etUmob.isEnabled = false
            }
        }

        ///BUTTON UPDATE ONCLICK
        btnUpdate.setOnClickListener {
            val stringRequest = object : StringRequest(
                Method.POST, const.COORDINATOR_UPDATE_URL,
                Response.Listener<String> { response ->
                    Toast.makeText(context,response,Toast.LENGTH_SHORT).show()
                    try {
                        if (response.trim { it <= ' ' }.equals(const.SUCCESS, ignoreCase = true)) {
                            Toast.makeText(rootView.context, "Update Succeccfully",Toast.LENGTH_LONG).show()
                        } else {

                        }
                    } catch (e: Exception) {
                        Toast.makeText(rootView.context, "Something went wrong! $e",Toast.LENGTH_LONG).show()
                    }
                },
                Response.ErrorListener { Toast.makeText(rootView.context, "Something went wrong!",Toast.LENGTH_LONG).show() }) {
                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String> {
                    val params = HashMap<String,String>()
                    params["username"] = etUname.text.toString()
                    params["useremail"] = etUemail.text.toString()
                    if(from==const.FROM_PARENT && role == const.ROLE_ADMIN)
                        departmentName = "MCA"
                    params["department"] = departmentName
                    params["uid"] = uid
                    params["mob"] = etUmob.text.toString()
                    return params
                }
            }
            val requestQueue = Volley.newRequestQueue(rootView.context)
            requestQueue.add(stringRequest)
        }
        return rootView
    }

    private fun setData() {
        val stringRequest = object : StringRequest(
            Method.POST, const.GETDATA_URL,
            Response.Listener<String> { response ->
                var jsonObject = JSONObject(response)
                try {
                    if(jsonObject.getString("log")=="success"){
                        val name = jsonObject.getString("username")
                        val email = jsonObject.getString("useremail")
                        val mob = jsonObject.getString("usermob")
                        departmentName = jsonObject.getString("department")
                        loader.setSpinnerDepartment(rootView,spinner,departmentName)

                        etUname.setText(name)
                        etUemail.setText(checkNull(email))
                        etUmob.setText(checkNull(mob))
                    }else{
                        Toast.makeText(rootView.context,response,Toast.LENGTH_LONG).show()
                    }
                }catch (e: Exception){}
            },
            Response.ErrorListener {
                Toast.makeText(rootView.context,"Something went wrong...",
                    Toast.LENGTH_SHORT).show()
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String,String>()
                params["uid"] = uid
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)

    }

    //chack the value is null or not-null==no data
    private fun checkNull(string: String): String {
        return if(string=="null") "" else string
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        departmentName = spinner.getItemAtPosition(position).toString()
    }
    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
