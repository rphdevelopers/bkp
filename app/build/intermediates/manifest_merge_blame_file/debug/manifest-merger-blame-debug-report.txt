1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.rphdevelopers.ritsports"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="21"
8-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:5:5-67
11-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:5:22-64
12
13    <application
13-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:7:5-28:19
14        android:allowBackup="true"
14-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:8:9-35
15        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
15-->[androidx.core:core:1.1.0] /home/zshadow/.gradle/caches/transforms-2/files-2.1/5b454e4f1a30a31f78d2352c725b41cb/core-1.1.0/AndroidManifest.xml:24:18-86
16        android:debuggable="true"
17        android:icon="@mipmap/ic_launcher"
17-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:9:9-43
18        android:label="@string/app_name"
18-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:10:9-41
19        android:roundIcon="@mipmap/ic_launcher_round"
19-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:11:9-54
20        android:supportsRtl="true"
20-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:12:9-35
21        android:testOnly="true"
22        android:theme="@style/AppTheme" >
22-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:13:9-40
23        <activity
23-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:14:9-16:57
24            android:name="com.rphdevelopers.ritsports.ScheduleActivity"
24-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:16:13-45
25            android:noHistory="true" />
25-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:15:13-37
26        <activity android:name="com.rphdevelopers.ritsports.UpdateCoordinator" />
26-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:17:9-55
26-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:17:19-52
27        <activity
27-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:18:9-20:66
28            android:name="com.rphdevelopers.ritsports.SignInActivity"
28-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:19:13-43
29            android:theme="@style/Theme.AppCompat.NoActionBar" />
29-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:20:13-63
30        <activity android:name="com.rphdevelopers.ritsports.MainActivity" >
30-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:21:9-27:20
30-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:21:19-47
31            <intent-filter>
31-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:22:13-26:29
32                <action android:name="android.intent.action.MAIN" />
32-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:23:17-69
32-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:23:25-66
33
34                <category android:name="android.intent.category.LAUNCHER" />
34-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:25:17-77
34-->/home/zshadow/AndroidStudioProjects/RITSports/app/src/main/AndroidManifest.xml:25:27-74
35            </intent-filter>
36        </activity>
37    </application>
38
39</manifest>
